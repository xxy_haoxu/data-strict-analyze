$(function(){
	layui.use('laydate', function(){
  		var laydate = layui.laydate;
 	
		var type = getQueryString('type');
		var id = localStorage.getItem("editId");
		$('.back').click(function(){
			if(type == 'edit'){
				window.location.href = 'details.html?id='+id+'';
			}else{
				window.location.href = 'index.html';
			}
		});
		
		initTime();
		if(type == 'edit'){
			initPage();
		}
		
		
		function initPage(){
			$.ajax({
				url: beseUrl+'/epidemicmanage/peopleInformation?uuid='+id+'',
				type: 'get',
				success: function(res){
					$('#content input').each(function(){
						$(this).val(res[$(this).attr('id')])
					});
				},
				error: function(error){
					console.log(error);
				}
			});
		}
		
		$('#submit').click(function(){
		    if(!$('#YName').val()){ 
		       	alert("请输入姓名");  
		        return ;
		    } 
		    var params = {};
			$('#content input').each(function(){
				if($(this).attr('type') == 'data'){
					params[$(this).attr('id')] = $(this).val()==''?null:$(this).val();
				}else{
					params[$(this).attr('id')] = $(this).val();
				}
			});
			params.sex = $('#sex').val();
			if(type == 'edit'){
				params.uuid = id;
				onSubmit(beseUrl+'/epidemicmanage/peopleInformationChanged',params);
			}else{
				onSubmit(beseUrl+'/epidemicmanage/newPeopleInformation',params);
			}
		});
		
		function initTime(){
			laydate.render({
		    	elem: '#TelTime',
		    	type: 'datetime'
			});
			laydate.render({
			    elem: '#ToTime',
		    	type: 'datetime'
			});
			laydate.render({
			    elem: '#YTime',
		    	type: 'datetime'
			});
			laydate.render({
			    elem: '#SJTime',
		    	type: 'datetime'
			});
			laydate.render({
			    elem: '#CheckTime',
		    	type: 'datetime'
			});
			laydate.render({
			    elem: '#EntryTime',
		    	type: 'datetime'
			});
			laydate.render({
			    elem: '#WorkableTime',
		    	type: 'datetime'
			});
			laydate.render({
			    elem: '#VerificationTime',
		    	type: 'datetime'
			});
			laydate.render({
			    elem: '#TMQTime',
			    type: 'datetime',
			    range: true
			});
		}
		
		function onSubmit(url,params){
			$.ajax({
				url: url,
				type: 'post',
				contentType: 'application/json',
				data: JSON.stringify(params),
				success: function(res){
					if(res == 'SUCCESS'){
						window.location.href = 'index.html';
					}else{
						alert('提交错误，请稍后尝试')
					}
				},
				error: function(error){
					console.log(error);
					if(error.status == 200){
						if(error.responseText == 'SUCCESS'){
							window.location.href = 'index.html';
						}
					}
				}
			});
		}
	});
});
