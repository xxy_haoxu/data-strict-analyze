$(function(){
	layui.use(['form','layer','laydate','table','laytpl'],function(){
	    var form = layui.form,
	        layer = parent.layer === undefined ? layui.layer : top.layer,
	        $ = layui.jquery,
	        laydate = layui.laydate,
	        laytpl = layui.laytpl,
	        table = layui.table,
	      	laypage = layui.laypage,
	      	element = layui.element;
	    var tableIns;
	    var page = 1,pagesize = 15,totals=0;
	    var tableData = [];
	    //定时器
	    var timers;
	    
	    var pagesizes = Number(getQueryString('pagesize'));
	    if(pagesizes){
	    	pagesize = pagesizes;
	    }

	    searchList();
	    tableTool();
	    
	    // 导出
	    $('#export').click(function(){
			$('#myform').attr('action',beseUrl+ '/export/downloadExcelFile')
	    	$('#myform').submit();
		});
	    
	    $('#upload').click(function(){
	    	$('#uploadSubmit').removeClass('layui-btn-disabled');
	    	$('#filePath').val('');
	    	$('#file').val('');
	    	$('.tip').html('');
	    	$('#progress').hide();
			$('#progressID').html('');
	    	var content = $('#uploadBox');
	    	layer.open({
			  	type: 1,
			  	title:'导入',
			  	id: 'assembly',
			  	skin: 'layui-layer-rim', //加上边框
			  	area: ['420px', '250px'], //宽高
			  	content: content
			});
	    });
	    
	    $('#searchBox input').keypress(function (e) {
            if (e.which == 13) {
				page = 1;
				length = 15;
                searchList();
            }
		});
	    /**
	     * 获取表格数据
	     */
	    function searchList(){
	    	var params = {
	    		'YName': $('#name').val(),
	    		'IDCard': $('#idNumber').val(),
	    		'Tel': $('#cellPhoneNumber').val(),
	    		'start': (page-1)*pagesize,
	    		'length': pagesize
	    	}
	    	$.ajax({
				url: beseUrl+'/epidemicmanage/allInformation',
				type: "post",
				contentType: 'application/json',
			    dataType: "json",
				data: JSON.stringify(params),
				success: function(res){
					tableData = res.list;
					totals = res.total;
					tableInit();
					showPage()
				},
				error: function(error){
					console.log(error);
				}
			});
	    }
	    /**
	     * 初始化表格
	     */
	    function tableInit(){
	    	tableIns = table.render({
		      	elem: '#table',
		      	id : "layTable",
		        height: 500,
		       	data:tableData,
				cellMinWidth : 100,
				loading:true,
			    page: false,
			    limit: pagesize,
			    limits: [15,30,50,100],
			    cols: [[ //表头
			        {field: 'YName', title: '姓名',width:'8%'},
			        {field: 'sex', title: '性别',width:'5%'},
			        {field: 'age', title: '年龄',width:'5%'},
			        {field: 'Nation', title: '民族',width:'8%'},
			        {field: 'IDCard', title: '身份证',width:'12%'},
			        {field: 'HJAddress', title: '户籍地址',width:'10%'},
			        {field: 'XAddress', title: '当前住址',width:'10%'},
			        {field: 'Tel', title: '手机号',width:'10%'},
			        {field: 'TelName', title: '手机开户姓名',width:'8%'},
			        {field: 'TelIDCard', title: '手机开户证件号',width:'12%'},
			        {field: 'TelOwn', title: '持机人姓名',width:'8%'},
					{field: 'sourceFile', title: '源文件',width:'8%'},
			        {title: '操作',templet:'#tableBar',align:"center",fixed: 'right',width:'5%'}
			    ]]
		    });
	    }
	    /**
	     * 初始化分页
	     */
	    function showPage(){
	    	laypage.render({
				elem:'pages',
				first: '首页',
			    last: '尾页',
			    prev: '<em>←</em>',
			    next: '<em>→</em>',
				count: totals ,//数据总数
				limit: pagesize,
				limits: [15,30,50,100],
				curr: location.hash.replace('#!page=', ''),
				layout: ['limit','count','prev','page','next','skip'],
				hash: 'page',
				jump: function(obj, first){
					if(!first){
						page = obj.curr;
						pagesize = obj.limit;
						searchList()
					}
				}
			});
	    }
	    function tableTool(){
	    	table.on('tool(table)', function(obj){
		        var layEvent = obj.event,
		            data = obj.data;
				window.location.href = 'details.html?id='+data.uuid+'&pagesize='+pagesize+'';
			});
	    }
	    
	    $('#filePath').click(function(){
	    	$('#file').click();
	    });
	    upload();
	    function upload(){
	    	$('#file').change(function(){
	    		var filePath = $(this)[0].files[0];
	    		$('#filePath').val(filePath.name);
    		});
	    }
	    $('#uploadSubmit').click(function(){
	    	if(!$(this).hasClass('layui-btn-disabled')){
	    		uploadSubmit();
	    	}
	    	$(this).addClass('layui-btn-disabled');
	    });
	    /**
	     * 上传文件
	     */
	    function uploadSubmit(){
	    	var filePath = $('#file')[0].files[0];
	    	var length = filePath.name.length;
	    	var suffix = filePath.name.substring(length-5,length);
    		if(suffix.indexOf('xlsx') == -1 && (suffix.indexOf('xls') == -1)){
    			$('.tip').html('请上传Excel文件').fadeIn().fadeOut(5000);
    			return ;
    		}
			debugger;
    		var timestamp = new Date().getTime();
    		var formData = new FormData();
    		formData.append('progressId',timestamp);
    		var headIndex = $('#headIndex').val();
    		if(null == headIndex || headIndex == ''){
                formData.append('headIndex',0);
    		} else{
                formData.append('headIndex',headIndex);
    		}
    		formData.append('file',filePath);
    		$.ajax({
				url: beseUrl+'/upload/yqupload',
				type: 'post',
		        data: formData,
		        cache: false,
		        contentType: false,
    			processData: false,
				success: function(res){
					alert(res.msg)
				},
				error: function(error){
					console.log(error);
				}
			});
			$('#uploadSubmit').removeClass('layui-btn-disabled');
			setTimeout(function () {
			    setIntervalFun(timestamp);
			}, 1000);
	    }
	    /**
	     * 获取上传文件进度
	     * @param {Object} id
	     */
	    function getProgress(id){
	    	var formData = new FormData();
    		formData.append('progressId',id);
    		$.ajax({
				url: beseUrl+'/upload/progress',
				type: 'post',
		        data: formData,
		        cache: false,
		        contentType: false,
    			processData: false,
				success: function(res){
				    if(res.status == true){
				    	$('#progress').show();
				    	$('#progressID').html(res.msg);
				    	if(res.over == true){
				    		clearInterval(timers);
				    		layer.closeAll();
				    		searchList();
				    	}
				    }else{
				    	clearInterval(timers);
				    	$('.tip').html('文件上传失败，请重新尝试').fadeIn().fadeOut(5000);
				    }
				},
				error: function(error){
					clearInterval(timers);
					console.log(error);
				}
			})
	    }
	    function setIntervalFun(id){
	    	timers = setInterval(function(){
		        getProgress(id)
		    }, 1000);
	    }
	});
});
