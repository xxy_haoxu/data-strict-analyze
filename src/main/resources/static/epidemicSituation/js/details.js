$(function(){
	var id = getQueryString('id');
	var pagesize = getQueryString('pagesize');
	$('#edit').click(function(){
		localStorage.setItem("editId",id);
    	window.location.href = 'edit.html?type=edit';
    });
    $('#back').click(function(){
    	window.location.href = 'index.html?pagesize='+pagesize+'';
    });
    initPage();
    function initPage(){
    	$.ajax({
			url: beseUrl+'/epidemicmanage/peopleInformation?uuid='+id+'',
			type: 'get',
			success: function(res){
				$('#content .values').each(function(){
					$(this).html(res[$(this).attr('id')]);
					$(this).attr('title',res[$(this).attr('id')]);
				});
			},
			error: function(error){
				console.log(error);
			}
		});
    }
});


