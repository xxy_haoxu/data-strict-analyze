$(function(){
	layui.use(['form','layer','laydate','table','laytpl'],function(){
	    var form = layui.form,
	        layer = parent.layer === undefined ? layui.layer : top.layer,
	        $ = layui.jquery,
	        laydate = layui.laydate,
	        laytpl = layui.laytpl,
	        table = layui.table;
	      	laypage = layui.laypage;
	    var tableIns;
	    var page = 1,pagesize = 15,totals=0;
	    var tableData = [];
	    
	    var timers;
	    
	    var pagesizes = Number(getQueryString('pagesize'));
	    if(pagesizes){
	    	pagesize = pagesizes;
	    }

	    searchList();
	    tableTool();
	       
	    $('#file').change(function(){
    		var filePath = $(this)[0].files[0];
    		$('#filePath').val(filePath.name);
		});
	    
		$('#export').click(exportTable);
		
	    $('#upload').click(function(){
	    	$('#uploadSubmit').removeClass('layui-btn-disabled');
	    	$('#filePath').val('');
	    	$('#file').val('');
	    	$('.tip').html('');
	    	$('#progress').hide();
			$('#progressID').html('');
	    	var content = $('#uploadBox');
	    	layer.open({
			  	type: 1,
			  	title:'导入',
			  	id: 'assembly',
			  	skin: 'layui-layer-rim', //加上边框
			  	area: ['420px', '250px'], //宽高
			  	content: content
			});
	    });
		$('#searchBox input').keypress(function (e) {
			if (e.which == 13) {
				page = 1;
				length = 15;
				searchList();
			}
		});
	    function searchList(){
	    	var params = {
	    		'start': (page-1)*pagesize,
	    		'length': pagesize,
				'ZMName':$('#ZMName').val(),
				'ZMIDcard':$('#ZMIDcard').val(),
				'ZMTel':$('#ZMTel').val()
	    	};
	    	$.ajax({
				url: beseUrl+'/epidemicmanage/allBurmaInformation',
				type: "post",
				contentType: 'application/json',
			    dataType: "json",
				data: JSON.stringify(params),
				success: function(res){
					tableData = res.list;
					totals = res.total;
					tableInit();
					showPage()
				},
				error: function(error){
					console.log(error);
				}
			});
	    }
	    
	    function tableInit(){
	    	tableIns = table.render({
		      	elem: '#table',
		      	id : "layTable",
		        height: 500,
		       	data:tableData,
				loading:true,
			    page: false,
			    limit: pagesize,
			    limits: [15,30,50,100],
			    cols: [[ //表头
			        {field: 'ZMName', title: '姓名',width:'100'},
			        {field: 'ZMSex', title: '性别',width:'50'},
			        {field: 'ZMAge', title: '年龄',width:'50'},
			        {field: 'ZMIDcard', title: '身份证',width:'150'},
			        {field: 'ZMBirth', title: '出生年月',width:'100'},
			        {field: 'ZMTel', title: '联系电话',width:'100'},
			        {field: 'ZMWechat', title: '微信',width:'100'},
			        {field: 'ZMQQ', title: 'QQ',width:'100'},
			        {field: 'ZMEmail', title: '电子邮箱',width:'100'},
			        {field: 'ZMType', title: '人员类型',width:'100'},
			        {field: 'ZMRenyuan', title: '关联人员',width:'100'},
			        {field: 'ZMguanxi', title: '关联人员关系',width:'100'},
			        {field: 'ZMGuiji', title: '轨迹信息',width:'100'},
			        {field: 'ZMCar', title: '持有车辆',width:'100'},
			        {field: 'ZMHouse', title: '持有房产',width:'100'},
			        {field: 'ZMBCard', title: '持有银行卡',width:'100'},
			        {field: 'ZMBeizhu', title: '备注',minWidth:'150'},
					{field: 'sourceFile', title: '源文件',minWidth:'150'},
			        {title: '操作',templet:'#tableBar',align:"center",fixed: 'right',width:'50'}
			    ]]
		    });
	    }
	    function showPage(){
	    	laypage.render({
				elem:'pages',
				first: '首页',
			    last: '尾页',
			    prev: '<em>←</em>',
			    next: '<em>→</em>',
				count: totals ,//数据总数
				limit: pagesize,
				limits: [15,30,50,100],
				curr: location.hash.replace('#!page=', ''),
				layout: ['limit','count','prev','page','next','skip'],
				hash: 'page',
				jump: function(obj, first){
					if(!first){
						page = obj.curr;
						pagesize = obj.limit;
						searchList()
					}
				}
			});
	    }
	    function tableTool(){
	    	table.on('tool(table)', function(obj){
		        var layEvent = obj.event,
		            data = obj.data;
				window.location.href = 'myanmarChinaDetails.html?id='+data.uuid+'&pagesize='+pagesize+'';
			});
	    }
	    
	    function exportTable(){
	    	$('#myform').attr('action',beseUrl+ '/export/downloadExcelFile')
	    	$('#myform').submit();
	    }
	    
	    $('#filePath').click(function(){
	    	$('#file').click();
	    });
	    upload();
	    function upload(){
	    	$('#file').change(function(){
	    		var filePath = $(this)[0].files[0];
	    		$('#filePath').prop("value",filePath.name);
    		});
	    }
	    $('#uploadSubmit').click(function(){
	    	if(!$(this).hasClass('layui-btn-disabled')){
	    		uploadSubmit();
	    	}
	    	$(this).addClass('layui-btn-disabled');
	    });
	    function uploadSubmit(){
	    	var filePath = $('#file')[0].files[0];
    		var length = filePath.name.length;
            var suffix = filePath.name.substring(length-5,length);
            if(suffix.indexOf('xlsx') == -1 && (suffix.indexOf('xls') == -1)){
                $('.tip').html('请上传Excel文件').fadeIn().fadeOut(5000);
                return ;
            }
    		var timestamp = new Date().getTime();
    		var formData = new FormData();
    		formData.append('progressId',timestamp);
    		var headIndex = $('#headIndex').val();
			if(null == headIndex || headIndex == ''){
				formData.append('headIndex',0);
			} else{
				formData.append('headIndex',headIndex);
			}
    		formData.append('file',filePath);
    		$.ajax({
				url: beseUrl+'/upload/zmupload',
				type: 'post',
		        data: formData,
		        cache: false,
		        contentType: false,
    			processData: false,
				success: function(res){},
				error: function(error){
					console.log(error);
				}
			});
			$('#uploadSubmit').removeClass('layui-btn-disabled');
			setTimeout(function () {
			    setIntervalFun(timestamp);
			}, 1000);
	    }
	    function getProgress(id){
	    	var formData = new FormData();
    		formData.append('progressId',id);
    		$.ajax({
				url: beseUrl+'/upload/progress',
				type: 'post',
		        data: formData,
		        cache: false,
		        contentType: false,
    			processData: false,
				success: function(res){
				    if(res.status == true){
				    	$('#progress').show();
				    	$('#progressID').html(res.msg);
				    	if(res.over == true){
				    		clearInterval(timers);
				    		layer.closeAll();
				    		searchList();
				    	}
				    }else{
				    	clearInterval(timers);
				    	$('.tip').html('文件上传失败，请重新尝试').fadeIn().fadeOut(5000);
				    }
				},
				error: function(error){
					clearInterval(timers);
					console.log(error);
				}
			})
	    }
	    function setIntervalFun(id){
	    	timers = setInterval(function(){
		        getProgress(id)
		    }, 1000);
	    }
	    
	});
});
