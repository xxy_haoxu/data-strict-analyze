$(function(){
	layui.use('laydate', function(){
  		var laydate = layui.laydate;
 	
		var type = getQueryString('type');
		var id = localStorage.getItem("myanmarChinaEditId");
		$('.back').click(function(){
			if(type == 'edit'){
				window.location.href = 'myanmarChinaDetails.html?id='+id+'';
			}else{
				window.location.href = 'myanmarChinaMesg.html';
			}
		});
		
		initTime();
		if(type == 'edit'){
			initPage();
		}
		
		
		function initPage(){
			$.ajax({
				url: beseUrl+'/epidemicmanage/peopleZMInformation?uuid='+id+'',
				type: 'get',
				success: function(res){
					$('#content input').each(function(){
						$(this).val(res[$(this).attr('id')])
					});
				},
				error: function(error){
					console.log(error);
				}
			});
		}
		
		$('#submit').click(function(){
		    if(!$('#ZMName').val()){ 
		       	alert("请输入姓名");  
		        return ;
		    } 
		    var params = {};
			$('#content input').each(function(){
				if($(this).attr('type') == 'data'){
					params[$(this).attr('id')] = $(this).val()==''?null:$(this).val();
				}else{
					params[$(this).attr('id')] = $(this).val();
				}
			});
			params.sex = $('#ZMSex').val();
			if(type == 'edit'){
				params.uuid = id;
				onSubmit(beseUrl+'/epidemicmanage/peopleZMInformationChanged',params);
			}else{
				onSubmit(beseUrl+'/epidemicmanage/newZMPeopleInformation',params);
			}
		});
		
		function initTime(){
			laydate.render({
		    	elem: '#ZMBirth',
		    	type: 'date'
			});
		}
		
		function onSubmit(url,params){
			$.ajax({
				url: url,
				type: 'post',
				contentType: 'application/json',
				data: JSON.stringify(params),
				success: function(res){
					if(res == 'SUCCESS'){
						window.location.href = 'myanmarChinaMesg.html';
					}else{
						alert('提交错误，请稍后尝试')
					}
				},
				error: function(error){
					console.log(error);
					if(error.status == 200){
						if(error.responseText == 'SUCCESS'){
							window.location.href = 'myanmarChinaMesg.html';
						}
					}
				}
			});
		}
	});
});
