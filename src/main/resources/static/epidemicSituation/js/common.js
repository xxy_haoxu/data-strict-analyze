var beseUrl = '/springbootdemo';

function getQueryString(name) {
	var result = window.location.search.match(new RegExp("[\?\&]" + name + "=([^\&]+)", "i"));
	if(result == null || result.length < 1) {
		return "";
	}
	return result[1];
}
function formatTen(num) { 
    return num > 9 ? (num + "") : ("0" + num); 
}
function formatDate(date) { 
    var date = new Date(date)
    var year = date.getFullYear(); 
    var month = date.getMonth() + 1; 
    var day = date.getDate(); 
    return year + "-" + formatTen(month) + "-" + formatTen(day); 
} 

 