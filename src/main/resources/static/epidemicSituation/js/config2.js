$(function(){
	layui.use(['form','layer','laydate','table','laytpl','element'],function(){
	    var form = layui.form,
	        layer = parent.layer === undefined ? layui.layer : top.layer,
	        $ = layui.jquery,
	        laydate = layui.laydate,
	        laytpl = layui.laytpl,
	        table = layui.table,
	      	laypage = layui.laypage,
	      	element = layui.element;
	    var tableIns;
	    var page = 1,pagesize = 15,page1 = 1,pagesize1 = 10,assemblyTableDataTotals = moduleTableTotals =0;
	    var assemblyTableData = [],moduleTable = [];
	    var checkedID = [];
	    //模型对应的组件id
	    var elementIds = [];
	    //表头
	    var assemblyTableCols = [[ 
	        {field: 'elementName', title: '组件名称',width:'30%'},
	       {field: 'fieldValue', title: '属性名称',width:'30%'},
	        {field: 'attributeValue', title: '属性值',width:'30%'},
	        {title: '操作',templet:'#assemblyTableBar',align:"center",fixed: 'right',width:'10%'}
	    ]];
	    var moduleTableCols = [[
	    	{field: 'id', title: '模型id'},
	        {field: 'modelName', title: '模型名称'},
	        {title: '操作',templet:'#moduleTableBar',align:"center",fixed: 'right',width:'15%'}
	    ]];

		assemblyQuery();
	    tableTool();
	    getAssemblySelect();
	    
	    $('#export').click(function(){
			$('#myform').attr('action',beseUrl+ '/export/downloadExcelFile');
			$('#elementIds').val(elementIds);
	    	$('#myform').submit();
		});
		
		$('#attributeName').change(function(){
	    	var types = $(this).find("option:selected").attr("type");
	    	$('#attributeValueBox').html('<span>属性值：</span>');
	    	$('#attributeValue').remove();
	    	if(types == 'list'){
	    		getAssemblyValue($(this).val());
	    	}else if(types == 'time'){
	    		var html='<input class="edit" id="attributeValue"/>';
	    		$('#attributeValueBox').append(html);
	    		laydate.render({
			    	elem: '#attributeValue',
			    	type: 'datetime',
			    	range: true
				});
	    	}else if(types == 'range'){
	    		var html='<input class="edit" id="attributeValue1"/>至<input class="edit" id="attributeValue2"/>';
	    		$('#attributeValueBox').append(html);
	    	}else{
	    		var html='<input class="edit" id="attributeValue"/>';
	    		$('#attributeValueBox').append(html);
	    	}
	    })
	    
	    
		//tab事件监听
		element.on('tab(config)', function(data){
			page = 1;
		    if(data.index == 0){
		    	assemblyQuery(true)
		    }else{
		    	moduleQuery(true);
		    }
		});
	    
	    $('#assemblyAdd').click(assemblyedit);
	    $('#assemblySubmit').click(assemblySbumit);
	    
	    $('#moduleAdd').click(moduleAssemblyedit);
	    $('#moduleAssemblySubmit').click(moduleAssemblySubmit);
	    
	    /**
	     * 获取组件表格数据
	     * @param {Object} flag
	     */
	    function assemblyQuery(flag){
	    	var params = {
	    		'type': 'zm',
	    		'start': (page-1)*pagesize,
	    		'length': pagesize
	    	};
		    searchList('/model/getElementList',params,'assemblyTable',flag);
	    }
	    /**
	     * 获取模型表格数据
	     * @param {Object} flag
	     */
	    function moduleQuery(flag){
	    	var params = {
	    		'type': 'zm',
	    		'start': (page-1)*pagesize,
	    		'length': pagesize
	    	};
	    	searchList('/model/getModelList',params,'moduleTable',flag);
	    }
	    
	    //获取表格数据
	    function searchList(urls,params,id,tab){
	    	$.ajax({
				url: beseUrl+ urls,
				type: "post",
				contentType: 'application/json',
			    dataType: "json",
				data: JSON.stringify(params),
				success: function(res){
					if(id == 'assemblyTable'){
						assemblyTableData = res.list;
						assemblyTableDataTotals = res.total;
						tableInit(id,assemblyTableCols,assemblyTableData);
						showPage(id,assemblyTableDataTotals,tab);
					}else{
						moduleTableData = res.list;
						moduleTableTotals = res.total;
						tableInit(id,moduleTableCols,moduleTableData);
						showPage(id,moduleTableTotals,tab)
					}
				},
				error: function(error){
					console.log(error);
				}
			});
	    }
	    /**
	     * 初始化表格
	     * @param {表格id} id
	     * @param {表头字段} cols
	     * @param {表格数据} data
	     */
	    function tableInit(id,cols,data){
	    	tableIns = table.render({
		      	elem: '#'+id+'',
		      	id : id,
		        height: 500,
		       	data: data,
				cellMinWidth : 100,
				loading: true,
			    page: false,
			    limit: pagesize,
			    limits: [15,30,50,100],
			    cols: cols
		    });
	    }
	    /**
	     * 表格分页
	     */
	    function showPage(id,totals,tab){
	    	var currentPage = tab==true?1:page;
	    	laypage.render({
				elem: id+'Page',
				first: '首页',
			    last: '尾页',
			    prev: '<em>←</em>',
			    next: '<em>→</em>',
				count: totals ,//数据总数
				limit: pagesize,
				curr: currentPage,
				layout: ['count','prev','page','next','skip'],
//				hash: 'page',
				jump: function(obj, first){
					if(!first){
						page = obj.curr;
						if(id == 'assemblyTable'){
							assemblyQuery();
						}else{
							moduleQuery();
						}
					}
				}
			});
	    }
	    
	    /**
	     * 表格操作
	     */
	    function tableTool(){
	    	table.on('tool(assemblyTable)', function(obj){
		        var layEvent = obj.event,
		            data = obj.data;
				var elementName = data.elementName;
				if(layEvent == 'delete'){
					layer.confirm('确认是否删除该组件？', {
					  	btn: ['确定','取消'] //按钮
					}, function(){
					  	assemblyDelete(data.id);
					});
				}else{
					assemblyedit('edit',data);
				}
			});
			table.on('tool(moduleTable)', function(obj){
		        var layEvent = obj.event,
		            data = obj.data;
				var modelName = data.modelName;
				if(layEvent == 'delete'){
					layer.confirm('确认是否删除该模型？', {
					  	btn: ['确定','取消'] //按钮
					}, function(){
					  	moduleDelete(data.id);
					});
				}else if(layEvent == 'details'){
					moduleAssemblyedit('edit',modelName);
				}else if(layEvent == 'statistics'){
					layer.load();
					getStatisticsTable(data.elementIds);
				}else if(layEvent == 'relevance'){
					layer.load();
					elementIds = data.elementIds;
					getPeopleShowTable(data.elementIds);
				}else{
					elementIds = data.elementIds;
					getPeopleShowTable(data.elementIds);
				}
			});
	    }
	    /**
	     * 打开组件编辑弹框
	     */
	    function assemblyedit(editFlag,data){
	    	$('#assemblyNameEdit').attr('editFlag','');
	    	$('#assemblyNameEdit').val('');
			$('#attributeName').val('');
			$('#attributeValue').val('');
	    	if(editFlag == 'edit'){
	    		$('#assemblyNameEdit').attr('editFlag','edit');
	    		$('#assemblyNameEdit').attr('oldElementName',data.elementName);
				$('#assemblyNameEdit').val(data.elementName);
				$('#attributeName').val(data.attributeName);
				$('#attributeValue').val(data.attributeValue);
				if(data.fieldType == 'time'){
					$('#attributeValueBox').html('<span>属性值：</span>');
		    		var html='<input class="edit" id="attributeValue"/>';
		    		$('#attributeValueBox').append(html);
		    		laydate.render({
				    	elem: '#attributeValue',
				    	type: 'datetime',
				    	range: true
					});
					var values = data.attributeValue.split('|');
					$('#attributeValue').val(values[0]+' - '+values[1]);
				}
				if(data.fieldType == 'range'){
					$('#attributeValueBox').html('<span>属性值：</span>');
		    		var html='<input class="edit" id="attributeValue1"/>至<input class="edit" id="attributeValue2"/>';
		    		$('#attributeValueBox').append(html);
		    		$('#attributeValue1').val(data.attributeValue.split('|')[0]);
		    		$('#attributeValue2').val(data.attributeValue.split('|')[1]);
		    	}
				assemblyLayer();
	    	}else{
	    		var html='<span>属性值：</span><input class="edit" id="attributeValue"/>';
		    	$('#attributeValueBox').html(html);
	    		assemblyLayer()
	    	}
	    }
	    function assemblyLayer(){
	    	var content = $('#assemblyEdit');
	    	layer.open({
			  	type: 1,
			  	title:'组件编辑',
			  	id: 'assembly',
			  	skin: 'layui-layer-rim', //加上边框
			  	area: ['420px', '250px'], //宽高
			  	content: content
			});
	    }
	    /**
	     * 组件编辑提交
	     */
	    function assemblySbumit(){
	    	if(!$('#assemblyNameEdit').val()){
	    		layer.tips('请填写组件名称', '#assemblyNameEdit', {
				  	tips: [1, '#FF5722'],
				  	time: 3000
				});
	    		return false;
	    	}
			if(!$('#attributeName').val()){
				layer.tips('请选择属性名称', '#attributeName', {
				  	tips: [1, '#FF5722'],
				  	time: 3000
				});
				return false;
			}
			if($('#attributeName').find("option:selected").attr("type") == 'range'){
	    		if(!$('#attributeValue1').val()){
					layer.tips('请填写属性值', '#attributeValue1', {
					  	tips: [1, '#FF5722'],
					  	time: 3000
					});
					return false;
				}
	    		if(!$('#attributeValue2').val()){
					layer.tips('请填写属性值', '#attributeValue2', {
					  	tips: [1, '#FF5722'],
					  	time: 3000
					});
					return false;
				}
	    	}else{
	    		if(!$('#attributeValue').val()){
					layer.tips('请填写属性值', '#attributeValue', {
					  	tips: [1, '#FF5722'],
					  	time: 3000
					});
					return false;
				}
	    	}
	    	var params = {
	    		'elementName': $('#assemblyNameEdit').val(),
	    		'attributeName': $('#attributeName').val(),
	    		'attributeValue': $('#attributeValue').val(),
	    		'fieldValue': $('#attributeName').find("option:selected").attr("name"),
	    		'type': 'zm'
	    	}
	    	if($('#attributeName').find("option:selected").attr("type") == 'time'){
	    		params.attributeValue = $('#attributeValue').val().split(' - ').join('|')
	    	}
	    	if($('#attributeName').find("option:selected").attr("type") == 'range'){
	    		params.attributeValue = $('#attributeValue1').val()+'|'+$('#attributeValue2').val()
	    	}
	    	var urls = '/model/addElementInfo';
	    	if($('#assemblyNameEdit').attr('editFlag') == 'edit'){
	    		urls = '/model/updateElementInfo';
	    		params.oldElementName = $('#assemblyNameEdit').attr('oldElementName');
	    	}
	    	$.ajax({
				url: beseUrl+urls,
				type: "post",
				contentType: 'application/json',
				data: JSON.stringify(params),
				success: function(res){
					if(res == 'SUCCESS'){
						layer.closeAll();
						assemblyQuery();
					}else{
						layer.msg(res);
					}
				},
				error: function(error){
					console.log(error);
				}
			});
	    }
	    /**
	     * 组件删除
	     * @param {组件id} elementName
	     */
	    function assemblyDelete(id){
	    	$.ajax({
				url: beseUrl+'/model/deleteElementInfo?id='+id,
				type: "post",
				contentType: 'application/json',
				data: id,
				success: function(res){
					if(res == 'SUCCESS'){
						layer.closeAll();
						assemblyQuery();
					}else{
						layer.msg(res);
					}
				},
				error: function(error){
					console.log(error);
				}
			});
	    }
	    /**
	     * 获取模型对应的组件
	     */
	    function moduleAssemblyedit(editFlag,modelName){
	    	var params = { 'type': 'zm' };
	    	$('#moduleName').attr('editFlag','');
	    	$('#moduleName').val('');
	    	if(editFlag == 'edit'){
	    		$('#moduleName').val(modelName);
	    		$('#moduleName').attr('oldModelName',modelName);
	    		$('#moduleName').attr('editFlag','edit');
	    		params.modelName = modelName;
	    	}
	    	$.ajax({
				url: beseUrl+'/model/getElementList',
				type: "post",
				contentType: 'application/json',
				dataType: "json",
				data: JSON.stringify(params),
				success: function(res){
					tableData = res.list;
					totals = res.total;
					checkedID = [];
					if(res.list.length > 0){
						for(let i=0;i<res.list.length;i++){
	    					if(res.list[i].LAY_CHECKED == true){
								checkedID.push(res.list[i].id);
							}
	    				}
					}
					tables();
					moduleLayer();
				},
				error: function(error){
					console.log(error);
				}
			});
	    }
	    
	    function moduleLayer(){
	    	var content = $('#moduleEdit');
		    layer.open({
			  	type: 1,
			  	title: '模型编辑',
			  	id: 'module',
			  	skin: 'layui-layer-rim', //加上边框
			  	area: ['600px', '650px'], //宽高
			  	content: content
			});
	    }
	    
	    function tables(){
	    	var cols = [[ 
	    		{type:'checkbox',width: '50'},
		        {field: 'elementName', title: '组件名称',width:'150'},
		        {field: 'fieldValue', title: '属性名称',width:'200'},
		        {field: 'attributeValue', title: '属性值',width:'155'}
		    ]];
	    	tableIns = table.render({
		      	elem: '#moduleAssemblyTable',
		      	id : 'moduleAssemblyTable',
		        height: 500,
		       	data: tableData,
				cellMinWidth : 100,
				loading: true,
			    page: true,
			    limits: [15,30,50,100],
			    cols: cols,
			    done: function(res,data,count){
			    	table.on('checkbox(moduleAssemblyTable)',function(obj){
			    		//单行操作
			    		if(obj.type == 'one'){
			    			if(obj.checked == true){
			    				checkedID.push(obj.data.id);
			    			}else{
			    				var index = jQuery.inArray(obj.data.id,checkedID);
			    				checkedID.splice(index,1);
			    			}
			    		}else{
			    			var checkData = table.checkStatus('moduleAssemblyTable').data;
			    			if(checkData.length > 0){
			    				for(let i=0;i<checkData.length;i++){
			    					if(checkedID.indexOf(checkData[i].id) == -1){
			    						checkedID.push(checkData[i].id)
			    					}
			    				}
			    			}else{
			    				for(let i=0;i<res.data.length;i++){
			    					var index = jQuery.inArray(res.data[i].id,checkedID);
			    					checkedID.splice(index,1);
			    				}
			    			}
			    		}
			    	});
			    }
		    });
	    }
	    
	    /**
	     * 获取组件属性名
	     */
	    function getAssemblySelect(){
	    	$.ajax({
				url: beseUrl+'/model/getFieldList?type=zm',
				type: "post",
				contentType: 'application/json',
				data: {},
				success: function(res){
					if(res.length > 0){
						var html = '';
						for(let i=0;i<res.length;i++){
							html += '<option value="'+res[i].fieldName+'" name="'+res[i].fieldValue+'" type="'+res[i].fieldType+'">'+res[i].fieldValue+'</option>';
						}
						$('#attributeName').append(html);
					}
				},
				error: function(error){
					console.log(error);
				}
			});
	    }
	    /**
	     * 获取组件属性值
	     * @param {Object} fieldName
	     */
	    function getAssemblyValue(fieldName){
	    	$.ajax({
				url: beseUrl+'/model/getFieldValueList?fieldName='+fieldName+'&type=zm',
				type: "post",
				contentType: 'application/json',
				data: {fieldName:fieldName,type:'zm'},
				success: function(res){
					var html = '<select id="attributeValue">';
					if(res.length > 0){
						for(let i=0;i<res.length;i++){
							html += '<option value="'+res[i]+'">'+res[i]+'</option>';
						}
					}
					html += '</select>';
					$('#attributeValueBox').append(html);
				},
				error: function(error){
					console.log(error);
				}
			});
	    }
	    
	    /**
	     * 编辑模型
	     */
	    function moduleAssemblySubmit(){
	      	if(!$('#moduleName').val()){
	    		layer.tips('请填写模型名称', '#moduleName', {
				  	tips: [1, '#FF5722'],
				  	time: 3000
				});
	    		return false;
	    	}
			if(!checkedID || checkedID == ""){
				layer.tips('请选择组件', '#moduleName', {
				  	tips: [1, '#FF5722'],
				  	time: 3000
				});
				return false;
			}
	    	var urls = '/model/addModelInfo';
	    	var params = {
	    		modelName: $('#moduleName').val(),
	    		'type': 'zm',
	    		elementIds: checkedID.join(',')
	    	}
	    	if($('#moduleName').attr('editFlag') == 'edit'){
	    		urls = '/model/updateModelInfo';
	    		params.oldModelName = $('#moduleName').attr('oldModelName');
	    	}
	      	$.ajax({
				url: beseUrl+urls,
				type: "post",
				contentType: 'application/json',
				data: JSON.stringify(params),
				success: function(res){
					if(res == 'SUCCESS'){
						layer.closeAll();
						moduleQuery();
					}else{
						layer.msg(res);
					}
				},
				error: function(error){
					console.log(error);
				}
			});
	    }
	    /**
	     * 模型删除
	     * @param {组件id} elementName
	     */
	    function moduleDelete(id){
	    	$.ajax({
				url: beseUrl+'/model/deleteModelInfo?id='+id,
				type: "post",
				contentType: 'application/json',
				data: id,
				success: function(res){
					if(res == 'SUCCESS'){
						layer.closeAll();
						moduleQuery();
					}else{
						layer.msg(res);
					}
				},
				error: function(error){
					console.log(error);
				}
			});
	    }
	    /**
	     * 获取模型关联的人员
	     * @param {Object} elementIds
	     */
	    function getPeopleShowTable(elementIds){
	    	var params = {
	    		'elementIds': elementIds,
	    		'type': 'zm',
	    		'start': (page1-1)*pagesize1,
	    		'length': pagesize1
	    	};
	    	$.ajax({
				url: beseUrl+'/model/getAllPersonalList',
				type: "post",
				contentType: 'application/json',
				data: JSON.stringify(params),
				success: function(res){
					layer.closeAll();
					PeopleShowTable(res.list);
					showPage1(res.total,elementIds)
					peopleShowTableLayer();
				},
				error: function(error){
					console.log(error);
				}
			});
		}
		/**
	     * 获取组件统计数量
	     * @param {Object} elementIds
	     */
		function getStatisticsTable(elementIds){
	    	var params = {
	    		'type': 'zm',
	    		'elementIds': elementIds
	    	};
	    	$.ajax({
				url: beseUrl+'/model/getStatistics',
				type: "post",
				contentType: 'application/json',
				data: JSON.stringify(params),
				success: function(res){
					layer.closeAll();
					StatisticsTable(res);
					statisticsTableLayer();
				},
				error: function(error){
					console.log(error);
				}
			});
	    }
	    function PeopleShowTable(tableDatas){
	    	tableInss = table.render({
		      	elem: '#peopleShowTable',
		      	id : "PeopleShowTable",
		        height: 500,
		       	data:tableDatas,
				cellMinWidth : 100,
				loading:true,
			    page: false,
			    limit: pagesize1,
			    cols: [[ //表头
			        {field: 'ZMName', title: '姓名',width:'80'},
			        {field: 'ZMSex', title: '性别',width:'50'},
			        {field: 'ZMAge', title: '年龄',width:'50'},
			        {field: 'ZMIDcard', title: '身份证',width:'150'},
			        {field: 'ZMBirth', title: '出生年月',width:'100'},
			        {field: 'ZMTel', title: '联系电话',width:'100'},
			        {field: 'ZMWechat', title: '微信',width:'80'},
			        {field: 'ZMQQ', title: 'QQ',width:'80'},
			        {field: 'ZMEmail', title: '电子邮箱',width:'100'},
			        {field: 'ZMType', title: '人员类型',width:'100'},
			        {field: 'ZMRenyuan', title: '关联人员',width:'80'},
			        {field: 'ZMguanxi', title: '关联人员关系',width:'80'},
			        {field: 'ZMGuiji', title: '轨迹信息',width:'100'},
			        {field: 'ZMCar', title: '持有车辆',width:'100'},
			        {field: 'ZMHouse', title: '持有房产',width:'100'},
			        {field: 'ZMBCard', title: '持有银行卡',width:'100'},
			        {field: 'ZMBeizhu', title: '备注'},
			    ]]
		    });
	    }
	    function showPage1(totals,elementIds){
	    	laypage.render({
				elem:'peopleShowTablePage',
				first: '首页',
			    last: '尾页',
			    prev: '<em>←</em>',
			    next: '<em>→</em>',
				count: totals ,//数据总数
				limit: pagesize1,
				limits: [10,30,50,100],
				curr: location.hash.replace('#!page=', ''),
				layout: ['count','prev','page','next','skip'],
				hash: 'page',
				jump: function(obj, first){
					if(!first){
						page1 = obj.curr;
						getPeopleShowTable(elementIds)
					}
				}
			});
	    }
	    function peopleShowTableLayer(){
	    	var content = $('#people');
		    layer.open({
			  	type: 1,
			  	title: '模型关联',
			  	id: 'modulePeopleShows',
			  	skin: 'layui-layer-rim', //加上边框
			  	area: ['900px', '650px'], //宽高
			  	content: content
			});
		}
		function StatisticsTable(tableDatas){
	    	tableInss = table.render({
		      	elem: '#statisticsShowTable',
		      	id : "statisticsShowTable",
		        height: 500,
		       	data:tableDatas,
				cellMinWidth : 100,
				loading:true,
				page: false,
				totalRow: true,
			    cols: [[ //表头
			        {field: 'elementName', title: '组件名称',width:'400',unresize: true, totalRowText: '合计'},
					{field: 'count', title: '小计',width:'150',totalRow: true}
			    ]]
		    });
		}
		function statisticsTableLayer(){
	    	var content = $('#statistics');
		    layer.open({
			  	type: 1,
			  	title: '模型统计',
			  	id: 'statisticsPeopleShows',
			  	skin: 'layui-layer-rim', //加上边框
			  	area: ['600px', '650px'], //宽高
			  	content: content
			});
	    }
	});
});
