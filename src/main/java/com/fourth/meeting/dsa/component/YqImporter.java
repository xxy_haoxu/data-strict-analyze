package com.fourth.meeting.dsa.component;

import com.fourth.meeting.dsa.mapper.FieldInfoMapper;
import com.fourth.meeting.dsa.mapper.YqBaseInfoMapper;
import com.fourth.meeting.dsa.parser.entity.ExcelParseResult;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 疫情曹汝
 * @author Administrator
 */
@Component
public class YqImporter {

    /**
     * 字段配置表.
     */
    @Autowired
    private FieldInfoMapper fieldInfoMapper;

    /**
     * 疫情基础信息表maper.
     */
    @Autowired
    private YqBaseInfoMapper yqBaseInfoMapper;

    @Value("${system.update.yqignore}")
    private String yqUpdateIgnore;

    @Transactional
    public Map<String, Object> importData(List<ExcelParseResult> excelData) {
        Map<String,Object> rtn = Maps.newHashMap();
        if(CollectionUtils.isEmpty(excelData)){
            rtn.put("status",false);
            rtn.put("msg","解析数据为空");
            return rtn;
        }
        // 查询疫情表字段信息
        Map<String,String> yqFields = queryYqFieldInfo();
        excelData.forEach(alldata->{
            List<Map<String,Object>> sheetData = alldata.getExcelData();
            if(CollectionUtils.isEmpty(sheetData)){
                return;
            }
            List<Map<String,Object>> insertData = Lists.newArrayList();
            // excel文件名.
            String excelName = alldata.getExcelName();
            // 当前sheet名.
            String sheetName = alldata.getSheetName();
            // 文件源
            String sourceFile = String.format("%s-%s",excelName,sheetName);

            rtn.put("status",true);
            rtn.put("msg",String.format("%s%s","正在处理sheet:",sheetName));
            ProgressCache.addProgress(ProgressCache.getId(),rtn);
            sheetData.forEach(row->{
                if(MapUtils.isEmpty(row)){
                    return;
                }
                boolean dataRight = false;
                Map<String,Object> data = Maps.newHashMap();
                Set<Map.Entry<String, Object>> set = row.entrySet();
                for(Map.Entry<String,Object> entry : set){
                    String fieldValue = entry.getKey();
                    Object value = entry.getValue();
                    if(!yqFields.containsKey(fieldValue)){
                        continue;
                    }
                    if(StringUtils.equalsAny(fieldValue,"姓名","身份证号","手机号码")){
                        dataRight = true;
                    }
                    data.put(yqFields.get(fieldValue),value);
                }
                if(!dataRight){
                    return;
                }
                // 所有字段都是空值，那么也不导入.
                List<Object> vs = data.values().stream().filter(v->null != v && StringUtils.isNotBlank(Objects.toString(v))).collect(Collectors.toList());
                if(CollectionUtils.isEmpty(vs)){
                    return;
                }
                data.put("sourceFile",sourceFile);
                insertData.add(data);
            });
            if(CollectionUtils.isEmpty(insertData)){
                return;
            }
            // 保存数据
            saveToDb(insertData);
        });
        return rtn;
    }

    /**
     * 解析后的数据保存到数据库.
     * @param insertData
     */
    private void saveToDb(List<Map<String, Object>> insertData) {
        if(CollectionUtils.isEmpty(insertData)){
            return;
        }
        List<Map<String,Object>> newDataList = insertData.stream().filter(MapUtils::isNotEmpty).collect(Collectors.toList());
        Set<String> tableHeads = newDataList.get(0).keySet();
        List<List<Map<String,Object>>> parted = Lists.partition(newDataList,500);
        Map<String,Object> info = Maps.newHashMap();
        int saved = 0;
        int totalSize = CollectionUtils.size(newDataList);
        for (List<Map<String, Object>> part : parted) {
            if(CollectionUtils.isEmpty(part)){
                return;
            }
            List<Map<String,Object>> newPart = appendPersonInfo(part);
            saved += CollectionUtils.size(part);
            info.put("status",true);
            info.put("msg",String.format("%s%s%s%s","数据入库:",saved,"/",totalSize));
            if(CollectionUtils.isEmpty(newPart)){
                ProgressCache.addProgress(ProgressCache.getId(),info);
                return;
            }
            yqBaseInfoMapper.saveBaseInfo(tableHeads,newPart);
            ProgressCache.addProgress(ProgressCache.getId(),info);
        }
    }

    /**
     * 如果是手机号或者身份证相同，那么根据手机号或者身份证号更新人员信息.
     * @param part excel解析得到的数据.
     * @return 不全在数据库中的数据.
     */
    private List<Map<String, Object>> appendPersonInfo(List<Map<String, Object>> part) {
        if(CollectionUtils.isEmpty(part)){
            return part;
        }
        // 过滤身份不为空的数据
        List<String> idcards = part.stream().map(row->Objects.toString(MapUtils.getObject(row,"IDCard"))).filter(StringUtils::isNotBlank).collect(Collectors.toList());
        // 过滤手机号不为空的数据
        List<String> phones = part.stream().map(row->Objects.toString(MapUtils.getObject(row,"Tel"))).filter(StringUtils::isNotBlank).collect(Collectors.toList());
        if(CollectionUtils.isEmpty(idcards) && CollectionUtils.isEmpty(phones)){
            return part;
        }
        // 根据身份证查询人员信息.
        List<Map<String,Object>> idcardBaseInfos = yqBaseInfoMapper.getInfoById(idcards);
        // 根据手机号查询人员信息.
        List<Map<String,Object>> phoneBaseInfos = yqBaseInfoMapper.getInfoByTel(phones);

        // 根据手机号更新.
        List<Map<String,Object>> newPart = updateByTels(part,phoneBaseInfos);
        // 根据身份证更新.
        newPart = updateByIdcards(newPart,idcardBaseInfos);
        return newPart;

    }

    /**
     * 根据手机号更新人员信息.
     * @param part excel解析得到的数据
     * @param phoneBaseInfos 根据手机号查询到的人员信息.
     * @return 根据手机号更新.
     */
    private List<Map<String, Object>> updateByTels(List<Map<String, Object>> part, List<Map<String, Object>> phoneBaseInfos) {
        if(CollectionUtils.isEmpty(phoneBaseInfos) || CollectionUtils.isEmpty(part)){
            return part;
        }
        Map<String,List<Map<String,Object>>> groupedByTel = phoneBaseInfos.stream().collect(Collectors.groupingBy(row->MapUtils.getString(row,"Tel")));
        // 执行更新，并返回待新增的数据
        return executeUpdate(part,groupedByTel,"Tel");
    }

    private List<Map<String, Object>> updateByIdcards(List<Map<String, Object>> part, List<Map<String, Object>> idcardBaseInfos) {
        if(CollectionUtils.isEmpty(idcardBaseInfos) || CollectionUtils.isEmpty(part)){
            return part;
        }
        // 根据身份证分组
        Map<String,List<Map<String,Object>>> groupedById = idcardBaseInfos.stream().collect(Collectors.groupingBy(row->MapUtils.getString(row,"IDCard")));
        // 执行更新，并返回待新增的数据
        return executeUpdate(part,groupedById,"IDCard");
    }


    /**
     * 更新处理
     * @param part 500调待处理数据.
     * @param groupedByKey 根据身份证号分组的数据.
     */
    private List<Map<String,Object>> executeUpdate(List<Map<String, Object>> part, Map<String, List<Map<String, Object>>> groupedByKey, String updateField) {
        List<Map<String,Object>> insertData = Lists.newArrayList();
        List<Map<String,Object>> updateData = Lists.newArrayList();
        part.forEach(row->{
            if(null == MapUtils.getObject(row,updateField) || StringUtils.isBlank(Objects.toString(MapUtils.getObject(row,updateField)))){
                insertData.add(row);
                return;
            }
            if(!groupedByKey.containsKey(Objects.toString(MapUtils.getObject(row,updateField)))){
                insertData.add(row);
                return;
            }
            List<Map<String,Object>> existRow = obtainExistRow(groupedByKey,row,updateField);
            updateOneRow(row,existRow,updateField);
            updateData.addAll(existRow);
        });
        if(CollectionUtils.isNotEmpty(updateData)){
            updateData.forEach(data->{
                if(MapUtils.isEmpty(data)){
                    return;
                }
                yqBaseInfoMapper.updateInfoByUuid(data);
            });
        }
        return insertData;
    }

    /**
     * 根据
     * @param groupedByKey 根据手机号或者身份证号group后的map.
     * @param row excel解析的新增的行.
     * @param updateField 手机号/身份证号字段.
     * @return
     */
    private List<Map<String, Object>> obtainExistRow(Map<String, List<Map<String, Object>>> groupedByKey, Map<String, Object> row, String updateField) {
        if(StringUtils.equals(updateField,"IDCard")){
            List<Map<String,Object>> idExist = groupedByKey.get(Objects.toString(MapUtils.getObject(row,updateField)));
            return idExist.stream().filter(MapUtils::isNotEmpty).collect(Collectors.toList());
        }

        List<Map<String,Object>> telExist = Lists.newArrayList();
        String rowPhone = MapUtils.getString(row,updateField);
        Set<String> phones = groupedByKey.keySet();
        if(StringUtils.equals(updateField,"Tel")){
            for(String phone: phones){
                if(StringUtils.isBlank(phone)){
                    continue;
                }
                if(StringUtils.contains(phone,rowPhone)){
                    List<Map<String,Object>> newAdd = groupedByKey.get(phone);
                    List<Map<String,Object>> newAdd1 = newAdd.stream().filter(MapUtils::isNotEmpty).collect(Collectors.toList());
                    telExist.addAll(newAdd1);
                }
            }
            return telExist;
        }
        return Lists.newArrayList();
    }

    /**
     * 数据库已有的数据更新.
     * @param row excel对应的行记录.
     * @param existRow 数据库已有的行记录.
     * @param updateField 本次更新依据的字段.
     */
    private void updateOneRow(Map<String, Object> row, List<Map<String, Object>> existRow, String updateField) {
        if(CollectionUtils.isEmpty(existRow)){
            return;
        }
        existRow.forEach(eRow->{
            updateOneRow(row,eRow,updateField);
        });
    }

    /**
     * 更新一行记录,row和eRow经过删选，两项内容必定是身份证或者手机号相同.
     * @param row excel中的一行.
     * @param eRow 在库中存在的行.
     */
    private void updateOneRow(Map<String, Object> row,Map<String, Object> eRow, String updateField){
        row.forEach((key,value)->{
            if(StringUtils.isBlank(key) || null == value || StringUtils.isBlank(Objects.toString(value))){
                return;
            }
            Object eValue = eRow.get(key);
            if(null == eValue || StringUtils.isBlank(Objects.toString(eValue))){
                eRow.put(key,value);
                return;
            }
            // updateField目前有身份证和手机号.
            if(StringUtils.equals(updateField,key)){
                return;
            }
            if(yqUpdateIgnore.contains(key)){
                return;
            }
            eValue = Objects.toString(eValue) + "|" + Objects.toString(value);
            eRow.put(key,eValue);
        });
    }

    /**
     * 查询字段信息.
     * @return
     */
    private Map<String, String> queryYqFieldInfo() {
        Map<String,Object> params = Maps.newHashMap();
        params.put("tableName","huizongjichu");
        List<Map<String,Object>> fields = fieldInfoMapper.queryFiledInfo(params);
        // 根据field_value分组
        Map<String,List<Map<String,Object>>> grouped = fields.stream().collect(Collectors.groupingBy(field->MapUtils.getString(field,"field_value")));
        // 转为一个map.
        Map<String,String> fieldMap = Maps.newHashMap();
        grouped.forEach((fieldValue,fieldInfo)->{
            if(CollectionUtils.isEmpty(fieldInfo)){
                return;
            }
            fieldMap.put(fieldValue, Objects.toString(fieldInfo.get(0).get("field_name")));
        });

        return fieldMap;
    }
}
