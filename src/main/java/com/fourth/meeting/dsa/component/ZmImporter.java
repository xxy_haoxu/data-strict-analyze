package com.fourth.meeting.dsa.component;

import com.fourth.meeting.dsa.mapper.FieldInfoMapper;
import com.fourth.meeting.dsa.mapper.ZmBaseInfolMapper;
import com.fourth.meeting.dsa.parser.entity.ExcelParseResult;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 中缅数据导入工具类.
 * @author Administrator
 */
@Component
public class ZmImporter {

    /**
     * 中缅mapper.
     */
    @Autowired
    private ZmBaseInfolMapper zmBaseInfolMapper;

    /**
     * 字段查询.
     */
    @Autowired
    private FieldInfoMapper fieldInfoMapper;

    @Value("${system.update.zmignore}")
    private String zmUpdateIgnore;

    @Transactional
    public Map<String, Object> importData(List<ExcelParseResult> excelData) {
        Map<String,Object> rtn = Maps.newHashMap();
        if(CollectionUtils.isEmpty(excelData)){
            rtn.put("status",false);
            rtn.put("msg","解析数据为空");
            return rtn;
        }
        // 查询疫情表字段信息
        Map<String,String> yqFields = queryYqFieldInfo();


        excelData.forEach(alldata->{
            List<Map<String,Object>> sheetData = alldata.getExcelData();
            if(CollectionUtils.isEmpty(sheetData)){
                return;
            }
            List<Map<String,Object>> insertData = Lists.newArrayList();
            // excel文件名.
            String excelName = alldata.getExcelName();
            // 当前sheet名.
            String sheetName = alldata.getSheetName();
            // 文件源
            String sourceFile = String.format("%s-%s",excelName,sheetName);
            rtn.put("status",true);
            rtn.put("msg",String.format("%s%s","正在处理sheet:",sheetName));
            sheetData.stream().parallel().forEach(row->{
                if(MapUtils.isEmpty(row)){
                    return;
                }
                boolean dataRight = false;
                Map<String,Object> data = Maps.newHashMap();
                Set<Map.Entry<String, Object>> set = row.entrySet();
                for(Map.Entry<String,Object> entry : set){
                    String fieldValue = entry.getKey();
                    Object value = entry.getValue();
                    if(!yqFields.containsKey(fieldValue)){
                        continue;
                    }
                    if(StringUtils.equalsAny(fieldValue,"姓名","身份证号","电话号码")){
                        dataRight = true;
                    }
                    data.put(yqFields.get(fieldValue),value);
                }
                if(!dataRight){
                    return;
                }
                data.put("sourceFile",sourceFile);
                insertData.add(data);
            });
            if(CollectionUtils.isEmpty(insertData)){
                return;
            }
            saveToDb(insertData);
        });

        rtn.put("status",true);
        rtn.put("msg","解析完成");
        return rtn;
    }

    /**
     * 解析后的数据保存到数据库.
     * @param insertData
     */
    private void saveToDb(List<Map<String, Object>> insertData) {
        if(CollectionUtils.isEmpty(insertData)){
            return;
        }
        List<Map<String,Object>> newDataList = insertData.stream().filter(MapUtils::isNotEmpty).collect(Collectors.toList());
        Set<String> tableHeads = insertData.get(0).keySet();
        List<List<Map<String,Object>>> parted = Lists.partition(newDataList,500);
        Map<String,Object> info = Maps.newHashMap();
        int saved = 0;
        int totalSize = CollectionUtils.size(newDataList);
        for (List<Map<String, Object>> part : parted) {
            if(CollectionUtils.isEmpty(part)){
                return;
            }
            List<Map<String,Object>> newPart = appendPersonInfo(part);
            saved += CollectionUtils.size(part);
            info.put("status",true);
            info.put("msg",String.format("%s%s%s%s","数据入库:",saved,"/",totalSize));
            if(CollectionUtils.isEmpty(newPart)){
                ProgressCache.addProgress(ProgressCache.getId(),info);
                return;
            }
            zmBaseInfolMapper.saveBaseInfo(tableHeads,part);
            ProgressCache.addProgress(ProgressCache.getId(),info);
        }
    }

    /**
     *
     * @param part
     * @return
     */
    private List<Map<String, Object>> appendPersonInfo(List<Map<String, Object>> part) {
        if(CollectionUtils.isEmpty(part)){
            return part;
        }
        // 过滤身份不为空的数据
        List<String> idcards = part.stream().map(row->Objects.toString(MapUtils.getObject(row,"ZMIDcard"))).filter(card->StringUtils.isNotBlank(card)).collect(Collectors.toList());
        // 过滤手机号不为空的数据
        List<String> phones = part.stream().map(row->Objects.toString(MapUtils.getObject(row,"ZMTel"))).filter(StringUtils::isNotBlank).collect(Collectors.toList());
        if(CollectionUtils.isEmpty(idcards)){
            return part;
        }
        List<Map<String,Object>> idcardbaseInfos = zmBaseInfolMapper.getInfoById(idcards);
        // 根据手机号查询人员信息.
        List<Map<String,Object>> phoneBaseInfos = zmBaseInfolMapper.getInfoByTel(phones);

        // 根据手机号更新.
        List<Map<String,Object>> newPart = updateByTels(part,phoneBaseInfos);
        // 根据身份证更新.
        newPart = updateByIdcards(newPart,idcardbaseInfos);
        return newPart;
    }

    /**
     * 根据手机号匹配是否重复信息.
     * @param part
     * @param phoneBaseInfos
     * @return
     */
    private List<Map<String, Object>> updateByTels(List<Map<String, Object>> part, List<Map<String, Object>> phoneBaseInfos) {
        if(CollectionUtils.isEmpty(phoneBaseInfos)){
            return part;
        }
        // 根据身份证分组
        Map<String,List<Map<String,Object>>> groupedById = phoneBaseInfos.stream().collect(Collectors.groupingBy(row->MapUtils.getString(row,"ZMTel")));
        // 执行更新，并返回待新增的数据
        return executeUpdate(part,groupedById,"ZMTel");
    }

    /**
     * 根据身份证匹配是否重复信息.
     * @param newPart
     * @param idcardbaseInfos
     * @return
     */
    private List<Map<String, Object>> updateByIdcards(List<Map<String, Object>> newPart, List<Map<String, Object>> idcardbaseInfos) {
        if(CollectionUtils.isEmpty(idcardbaseInfos)){
            return newPart;
        }
        // 根据身份证分组
        Map<String,List<Map<String,Object>>> groupedById = idcardbaseInfos.stream().collect(Collectors.groupingBy(row->MapUtils.getString(row,"ZMIDcard")));
        // 执行更新，并返回待新增的数据
        return executeUpdate(newPart,groupedById,"ZMIDcard");
    }

    /**
     * 更新处理
     * @param part 500调待处理数据.
     * @param groupedByKey 根据身份证号分组的数据.
     */
    private List<Map<String,Object>> executeUpdate(List<Map<String, Object>> part, Map<String, List<Map<String, Object>>> groupedByKey,String updateField) {
        List<Map<String,Object>> insertData = Lists.newArrayList();
        List<Map<String,Object>> updateData = Lists.newArrayList();
        part.forEach(row->{
            if(null == MapUtils.getObject(row,updateField) || StringUtils.isBlank(Objects.toString(MapUtils.getObject(row,updateField)))){
                insertData.add(row);
                return;
            }
            if(!groupedByKey.containsKey(Objects.toString(MapUtils.getObject(row,updateField)))){
                insertData.add(row);
                return;
            }
            List<Map<String,Object>> existRow = obtainExistRow(groupedByKey,row,updateField);
            updateOneRow(row,existRow,updateField);
            updateData.addAll(existRow);
        });
        if(CollectionUtils.isNotEmpty(updateData)){
            updateData.forEach(data->{
                if(MapUtils.isEmpty(data)){
                    return;
                }
                zmBaseInfolMapper.updateInfoByUuid(data);
            });
        }
        return insertData;
    }

    /**
     * 获取相同行的数据.
     * @param groupedByKey
     * @param row
     * @param updateField
     * @return
     */
    private List<Map<String, Object>> obtainExistRow(Map<String, List<Map<String, Object>>> groupedByKey, Map<String, Object> row, String updateField) {
        if(StringUtils.equals(updateField,"ZMIDcard")){
            List<Map<String,Object>> idExist = groupedByKey.get(Objects.toString(MapUtils.getObject(row,updateField)));
            return idExist.stream().filter(MapUtils::isNotEmpty).collect(Collectors.toList());
        }

        List<Map<String,Object>> telExist = Lists.newArrayList();
        String rowPhone = MapUtils.getString(row,updateField);
        Set<String> phones = groupedByKey.keySet();
        if(StringUtils.equals(updateField,"ZMTel")){
            for(String phone: phones){
                if(StringUtils.isBlank(phone)){
                    continue;
                }
                if(StringUtils.contains(phone,rowPhone)){
                    List<Map<String,Object>> newAdd = groupedByKey.get(phone);
                    List<Map<String,Object>> newAdd1 = newAdd.stream().filter(MapUtils::isNotEmpty).collect(Collectors.toList());
                    telExist.addAll(newAdd1);
                }
            }
            return telExist;
        }
        return Lists.newArrayList();
    }

    private void updateOneRow(Map<String, Object> row, List<Map<String, Object>> existRow, String updateField) {
        if(CollectionUtils.isEmpty(existRow)){
            return;
        }
        existRow.forEach(eRow->{
            updateOneRow(row,eRow);
        });
    }

    /**
     * 更新一行记录
     * @param row
     * @param eRow
     */
    private void updateOneRow(Map<String, Object> row,Map<String, Object> eRow){
        row.forEach((key,value)->{
            if(StringUtils.isBlank(key) || null == value || StringUtils.isBlank(Objects.toString(value))){
                return;
            }
            Object eValue = eRow.get(key);
            if(null == eValue || StringUtils.isBlank(Objects.toString(eValue))){
                eRow.put(key,value);
                return;
            }
            if(zmUpdateIgnore.contains(key)){
                return;
            }
            eValue = Objects.toString(eValue) + "|" + Objects.toString(value);
            eRow.put(key,eValue);
        });
    }

    /**
     * 查询字段信息.
     * @return
     */
    private Map<String, String> queryYqFieldInfo() {
        Map<String,Object> params = Maps.newHashMap();
        params.put("tableName","zmzong");
        List<Map<String,Object>> fields = fieldInfoMapper.queryFiledInfo(params);
        // 根据field_value分组
        Map<String,List<Map<String,Object>>> grouped = fields.stream().collect(Collectors.groupingBy(field->MapUtils.getString(field,"field_value")));
        // 转为一个map.
        Map<String,String> fieldMap = Maps.newHashMap();
        grouped.forEach((fieldValue,fieldInfo)->{
            if(CollectionUtils.isEmpty(fieldInfo)){
                return;
            }
            fieldMap.put(fieldValue, Objects.toString(fieldInfo.get(0).get("field_name")));
        });

        return fieldMap;
    }
}
