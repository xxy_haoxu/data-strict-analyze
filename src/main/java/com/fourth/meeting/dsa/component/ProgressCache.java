package com.fourth.meeting.dsa.component;

import com.google.common.collect.Maps;

import java.util.Map;
import java.util.UUID;

/**
 * 存放进度的缓存.
 * @author Administrator
 */
public class ProgressCache {

    /**
     * 当前线程的任务ID.
     */
    private static final ThreadLocal<String> CURRENT_TASK_ID = new ThreadLocal<String>();

    private static final Map<String,Map<String,Object>> IMPORT_PROGRESS = Maps.newHashMap();

    public static void addProgress(String uuid,Map<String,Object> progressDetail){
        IMPORT_PROGRESS.put(uuid,progressDetail);
    }

    public static Map<String,Object> getProgress(String uuid){
        return IMPORT_PROGRESS.get(uuid);
    }

    public static void releaseProgress(String uuid){
        IMPORT_PROGRESS.remove(uuid);
    }

    /**
     * 设置当前线程ID，并返回ID值.
     * @return
     */
    public static String setId(String id){
        CURRENT_TASK_ID.set(id);
        return id;
    }

    public static String getId(){
        return CURRENT_TASK_ID.get();
    }

    public static String createId(){
        String id = UUID.randomUUID().toString();
        return id;
    }
}
