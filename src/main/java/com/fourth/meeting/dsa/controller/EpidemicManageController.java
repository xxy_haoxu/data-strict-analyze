package com.fourth.meeting.dsa.controller;

import com.fourth.meeting.dsa.service.EpidemicManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@RestController
@RequestMapping("/epidemicmanage")
@ResponseBody
@CrossOrigin
public class EpidemicManageController {

    @Autowired
    private EpidemicManageService epidemicManageService;
    /**
     * 获取信息列表.
     * @param searchModel 搜索内容
     * @return List<Map<String, Object>>
     */
    @PostMapping("/allInformation")
    public Map<String, Object> getAllInformation(@RequestBody Map<String,Object> searchModel)
    {
    	return epidemicManageService.getAllInformation(searchModel);
    }
    /**
     * 新增人员信息.
     * @param people 人员信息
     * @return 成功/失败
     */
    @PostMapping("/newPeopleInformation")
    public String addPeopleInformation(@RequestBody Map<String, Object> people)
    {
    	return epidemicManageService.addPeopleInformation(people);
    }

    @PostMapping("/newZMPeopleInformation")
    public String addZMPeopleInformation(@RequestBody Map<String, Object> people)
    {
        return epidemicManageService.addZMPeopleInformation(people);
    }
    /**
     * 修改人员信息.
     * @param people 人员信息
     * @return 成功/失败
     */
    @PostMapping("/peopleInformationChanged")
    public String updatePeopleInformation(@RequestBody Map<String, Object> people)
    {
    	return epidemicManageService.updatePeopleInformation(people);
    }

    @PostMapping("/peopleZMInformationChanged")
    public String updateZMPeopleInformation(@RequestBody Map<String, Object> people)
    {
        return epidemicManageService.updateZMPeopleInformation(people);
    }
    /**
     * 查询当前人员信息.
     * @param uuid 人员信息
     * @return Map<String, Object>
     */
    @GetMapping("/peopleInformation")
    public Map<String, Object> getPeopleInformation(String uuid)
    {
    	return epidemicManageService.getPeopleInformation(uuid);
    }

    @GetMapping("/peopleZMInformation")
    public Map<String, Object> getZMPeopleInformation(String uuid)
    {
        return epidemicManageService.getZMPeopleInformation(uuid);
    }

    @DeleteMapping("/deletePeopleInformation")
    public String deletePeopleInformation(String id)
    {
    	return epidemicManageService.deletePeopleInformation(id);
    }

    @DeleteMapping("/deleteZMPeopleInformation")
    public String deleteZMPeopleInformation(String id)
    {
        return epidemicManageService.deleteZMPeopleInformation(id);
    }

    /**
     * 获取信息列表.
     * @return List<Map<String, Object>>
     */
    @PostMapping("/allBurmaInformation")
    public Map<String, Object> getAllBurmaInformation(@RequestBody Map<String, Object> map)
    {
        return epidemicManageService.getAllBurmaInformation(map);
    }
}
