package com.fourth.meeting.dsa.controller;

import com.fourth.meeting.dsa.component.ProgressCache;
import com.fourth.meeting.dsa.service.ExportFileService;
import com.fourth.meeting.dsa.service.ImportFileService;
import com.google.common.collect.Maps;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @author Administrator
 */
@RestController
@RequestMapping("/upload")
@CrossOrigin
public class UploadController {

    /**
     * 日志对象.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(UploadController.class);

    @Autowired
    private ExportFileService exportFileService;


    /**
     * 文件导入service.
     */
    @Autowired
    private ImportFileService importFileService;

    @PostMapping("yqupload")
    public String yqupload(@RequestParam("file") MultipartFile file, @RequestParam("headIndex") Integer headIndex,@RequestParam("progressId") String progressId,Integer dataIndex)
    {
        LOGGER.info("progressid:{}",progressId);
        importFileService.yqUpload(file,headIndex,dataIndex,progressId);
        return progressId;
    }

    @PostMapping("zmupload")
    public String zmupload(@RequestParam("file") MultipartFile file, @RequestParam("headIndex") Integer headIndex,@RequestParam("progressId") String progressId, Integer dataIndex)
    {
        importFileService.zmupload(file,headIndex,dataIndex,progressId);
        return progressId;
    }

    /**
     *
     * @param uuid
     * @return
     */
    @PostMapping("progress")
    public Map<String,Object> currentProgress(@RequestParam("progressId") String uuid){
        Map<String,Object> result = ProgressCache.getProgress(uuid);
        if(MapUtils.isEmpty(result)){
            return result;
        }
        Map<String,Object> copy = Maps.newHashMap(result);
        // 如果导入已经成功，那么将改进度从缓存中删除.
        boolean isOver = MapUtils.getBoolean(copy,"over",false);
        if(isOver){
            ProgressCache.releaseProgress(uuid);
        }
        return copy;
    }
}
