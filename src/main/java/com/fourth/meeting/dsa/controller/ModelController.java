package com.fourth.meeting.dsa.controller;

import com.fourth.meeting.dsa.service.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/model")
@ResponseBody
@CrossOrigin
public class ModelController {

    @Autowired
    private ModelService modelService;

    /**
     * 获取组件列表.
     * @return List<Map<String, Object>>
     */
    @PostMapping("/getElementList")
    public Map<String, Object> getElementList(@RequestBody Map<String, Object> map)
    {
    	return modelService.getElementList(map);
    }
    
    /**
     * 新增组件.
     * @return 成功/失败
     */
    @PostMapping("/addElementInfo")
    public String addElementInfo(@RequestBody Map<String, Object> map)
    {
    	return modelService.addElementInfo(map);
    }
    /**
     * 修改组件信息.
     * @return 成功/失败
     */
    @PostMapping("/updateElementInfo")
    public String updateElementInfo(@RequestBody Map<String, Object> map)
    {
    	return modelService.updateElementInfo(map);
    }

    @PostMapping("/deleteElementInfo")
    public String deleteElementInfo(String id)
    {
        return modelService.deleteElementInfo(id);
    }

    /**
     * 获取模块列表.
     * @return List<Map<String, Object>>
     */
    @PostMapping("/getModelList")
    public Map<String, Object> getModelList(@RequestBody Map<String, Object> map)
    {
        return modelService.getModelList(map);
    }

    /**
     * 新增模块.
     * @return 成功/失败
     */
    @PostMapping("/addModelInfo")
    public String addModelInfo(@RequestBody Map<String, Object> map)
    {
        return modelService.addModelInfo(map);
    }
    /**
     * 修改模块信息.
     * @return 成功/失败
     */
    @PostMapping("/updateModelInfo")
    public String updateModelInfo(@RequestBody Map<String, Object> map)
    {
        return modelService.updateModelInfo(map);
    }


    @PostMapping("/deleteModelInfo")
    public String deleteModelInfo(String id)
    {
        return modelService.deleteModelInfo(id);
    }


    @PostMapping("/getAllPersonalList")
    public Map<String, Object> getAllPersonalList(@RequestBody Map<String, Object> map)
    {
        String elementIds = (String) map.get("elementIds");
        Integer start= (int)map.get("start");
        Integer length= (int)map.get("length");
        String type= (String)map.get("type");
        return modelService.getAllPersonalList(elementIds,start,length,type);
    }
    /**
     * 获取组件统计.
     * @param map
     * @return
     */
    @PostMapping("/getStatistics")
    public List<Map<String, Object>> getStatistics(@RequestBody Map<String, Object> map)
    {
        return modelService.getStatistics(map);
    }

    /**
     * 获取字段列表，如果字段是下拉字段，返回下拉信息.
     * @return List<Map<String, Object>>
     */
    @PostMapping("/getFieldList")
    public List<Map<String, Object>> getFieldList(String type)
    {
        return modelService.getFieldList(type);
    }


    @PostMapping("/getFieldValueList")
    public List<String> getFieldValueList(String type, String fieldName) {
        return modelService.getFieldValueList(type, fieldName);
    }
}
