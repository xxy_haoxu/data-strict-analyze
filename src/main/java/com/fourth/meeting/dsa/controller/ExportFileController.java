package com.fourth.meeting.dsa.controller;


import com.fourth.meeting.dsa.service.ExportFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/export")
@CrossOrigin
public class ExportFileController {

    @Autowired
    private ExportFileService exportFileService;

    @ResponseBody
    @RequestMapping(value = "/downloadExcelFile", method = RequestMethod.GET)
    public void downloadExcelFile(HttpServletResponse response, String type, String elementIds)
    {
        exportFileService.downloadExcelFile(response, type,elementIds);
    }


}
