package com.fourth.meeting.dsa.model;

import org.springframework.stereotype.Component;

/**
 * @author： wangfan
 * @date： 2020/5/24
 */
@Component
public class SearchModel {
	/**
	 * 用于搜索的姓名.
	 */
	private String YName;
	/**
	 * 用于搜索的身份证.
	 */
	private String IDCard;
	/**
	 * 用于搜索的电话.
	 */
	private String Tel;
	/**
	 * 页码.
	 */
	private String page;
	/**
	 * 条数.
	 */
	private String size;
	public String getYName() {
		return YName;
	}
	public void setYName(String yName) {
		YName = yName;
	}
	public String getIDCard() {
		return IDCard;
	}
	public void setIDCard(String iDCard) {
		IDCard = iDCard;
	}
	public String getTel() {
		return Tel;
	}
	public void setTel(String tel) {
		Tel = tel;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public SearchModel(String yName, String iDCard, String tel, String page, String size) {
		super();
		YName = yName;
		IDCard = iDCard;
		Tel = tel;
		this.page = page;
		this.size = size;
	}
	public SearchModel() {
	}
	@Override
	public String toString() {
		return "SearchModel [YName=" + YName + ", IDCard=" + IDCard + ", Tel=" + Tel + ", page=" + page + ", size="
				+ size + "]";
	}


}