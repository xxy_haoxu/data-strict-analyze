package com.fourth.meeting.dsa.model;


import org.springframework.stereotype.Component;

@Component
public class PersonnelBaseInfoEntity {

    private String name;
    private String idNumber;
    private String sex;
    private String age;
    private String dateOfBirth;
    private String cellPhoneNumber;
    private String weChat;
    private String qqNumber;
    private String email;
    private String personnelType;
    private String currentAddress;

    @Override
    public String toString() {
        return "PersonnelBaseInfoEntity{" +
                "name='" + name + '\'' +
                ", idNumber='" + idNumber + '\'' +
                ", sex='" + sex + '\'' +
                ", age='" + age + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", cellPhoneNumber='" + cellPhoneNumber + '\'' +
                ", weChat='" + weChat + '\'' +
                ", qqNumber='" + qqNumber + '\'' +
                ", email='" + email + '\'' +
                ", personnelType='" + personnelType + '\'' +
                ", currentAddress='" + currentAddress + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getCellPhoneNumber() {
        return cellPhoneNumber;
    }

    public void setCellPhoneNumber(String cellPhoneNumber) {
        this.cellPhoneNumber = cellPhoneNumber;
    }

    public String getWeChat() {
        return weChat;
    }

    public void setWeChat(String weChat) {
        this.weChat = weChat;
    }

    public String getQqNumber() {
        return qqNumber;
    }

    public void setQqNumber(String qqNumber) {
        this.qqNumber = qqNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPersonnelType() {
        return personnelType;
    }

    public void setPersonnelType(String personnelType) {
        this.personnelType = personnelType;
    }

    public String getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(String currentAddress) {
        this.currentAddress = currentAddress;
    }
}
