package com.fourth.meeting.dsa.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ModelMapper
{
	List<Map<String, Object>> getElementList(@Param("map") Map<String, Object> map);

	int getElementListCount(@Param("map") Map<String, Object> map);

	List<Map<String, Object>> getElementListByIds(@Param("list") List<Integer> elementIdList);

	List<Map<String, Object>> getModelList(@Param("map") Map<String, Object> map);

	int getModelListCount(@Param("map") Map<String, Object> map);

	int addElementInfo(@Param("map") Map<String, Object> map);

	int addModelInfo(@Param("map") Map<String, Object> map);

	int updateElementInfo(@Param("map") Map<String, Object> map);

	int updateModelInfo(@Param("map") Map<String, Object> map);

	int deleteElementInfo(@Param("id") String id);

	int deleteModelInfo(@Param("id") String id);

	int getElementCountByName(@Param("elementName") String elementName, @Param("type") String type);

	int getModelCountByName(@Param("modelName") String modelName, @Param("type") String type);

	String getElementIdsByName(@Param("modelName") String modelName, @Param("type") String type);

	List<Map<String, Object>> getAllPersonalList(@Param("mapList") List<Map<String, Object>> elementMap, @Param("start") Integer start,
                                                 @Param("length") Integer length, @Param("tableName") String tableName);

	int getAllPersonalListCount(@Param("mapList") List<Map<String, Object>> elementMap, @Param("tableName") String tableName);

	List<Map<String, Object>> getFieldList(@Param("tableName") String tableName);

	List<String> getFieldValueList(@Param("tableName") String tableName, @Param("fieldName") String fieldName);

	/**
	 * 查询组件统计信息.
	 * @param elementMap
	 * @param tableName
	 * @return
	 */
	int getStatisticsCount(@Param("elementMap") Map<String, Object> elementMap, @Param("tableName") String tableName);
}
