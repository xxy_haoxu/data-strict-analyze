package com.fourth.meeting.dsa.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
@Mapper
public interface FieldInfoMapper {

    public List<Map<String,Object>> queryFiledInfo(@Param(("param")) Map<String, Object> param);
}
