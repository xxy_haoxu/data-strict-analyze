package com.fourth.meeting.dsa.mapper;


import com.fourth.meeting.dsa.model.PersonnelBaseInfoEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ExportFileMapper {

    List<Map<String, Object>> getAllBurmaInformation(@Param("elementMap") List<Map<String, Object>> elementMap);

    List<Map<String, Object>> getJichuPeopleInformation(@Param("elementMap") List<Map<String, Object>> elementMap);
}
