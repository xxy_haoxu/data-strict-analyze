package com.fourth.meeting.dsa.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Administrator
 */
@Mapper
public interface ZmBaseInfolMapper {

    /**
     * 保存疫情基础信息.
     * @param baseInfos 疫情基础信息.
     * @return 保存成功的数量.
     */
    int saveBaseInfo(@Param("tableHeads") Set<String> heads, @Param("baseInfos") List<Map<String, Object>> baseInfos);

    /**
     * 通过身份证取人员基础信息.
     * @param ids 身份证号码.
     * @return
     */
    List<Map<String,Object>> getInfoById(@Param("ids") List<String> ids);

    /**
     * 根据主键更新
     * @param updateData
     * @return
     */
    int updateInfoByUuid(@Param("updateData") Map<String, Object> updateData);

    /**
     * 根据手机号查询中缅人员信息.
     * @param phones
     * @return
     */
    List<Map<String, Object>> getInfoByTel(@Param("tels") List<String> phones);
}
