package com.fourth.meeting.dsa.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author： wangfan
 * @date： 2020/05/24
 */
@Mapper
public interface EpidemicManageMapper
{
	 /**
     * 获取信息列表.
     * @param searchModel 搜索内容
     * @return List<Map<String, Object>>
     */
	List<Map<String, Object>> getAllInformation(@Param("searchModel") Map<String, Object> searchModel);

	/**
	 * 获取列表总条数.
	 * @param searchModel 搜索内容
	 * @return
	 */
	int getAllInformationCount(@Param("searchModel") Map<String, Object> searchModel);

	/**
     * 新增人员信息.
     * @param people 人员信息
     * @return int
     */
	int addPeopleInformation(@Param("people") Map<String, Object> people);

	int addZMPeopleInformation(@Param("people") Map<String, Object> people);
	/**
     * 修改人员信息.
     * @param people 人员信息
     * @return 成功/失败
     */
	int updatePeopleInformation(@Param("people") Map<String, Object> people);

	int updateZMPeopleInformation(@Param("people") Map<String, Object> people);
	/**
     * 查询当前人员信息.
     * @return Map<String, Object>
     */
	Map<String, Object> getPeopleInformation(@Param("uuid") String id);

	Map<String, Object> getZMPeopleInformation(@Param("uuid") String id);

	int deletePeopleInformation(@Param("id") String id);

	int deleteZMPeopleInformation(@Param("id") String id);

	List<Map<String, Object>> getAllBurmaInformation(@Param("map") Map<String, Object> map);
	int getAllBurmaInformationCount(@Param("map") Map<String, Object> map);
}
