package com.fourth.meeting.dsa.service;


import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.params.ExcelExportEntity;
import com.fourth.meeting.dsa.component.ProgressCache;
import com.fourth.meeting.dsa.mapper.ExportFileMapper;
import com.fourth.meeting.dsa.mapper.FieldInfoMapper;
import com.fourth.meeting.dsa.mapper.ModelMapper;
import com.fourth.meeting.dsa.model.PersonnelBaseInfoEntity;
import com.fourth.meeting.dsa.parser.ExcelParseTool;
import com.fourth.meeting.dsa.parser.entity.ExcelParseResult;
import com.fourth.meeting.dsa.utils.ExportUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ExportFileService {

    @Autowired
    private ExportFileMapper exportFileMapper;


    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private FieldInfoMapper fieldInfoMapper;

    /**
     * 新导出.
     * @param response
     * @param type
     * @param elementIds
     */
    public void downloadExcelFile(HttpServletResponse response, String type,String elementIds)
    {
        List<Integer> elementIdList =Lists.newArrayList();

        if(StringUtils.isNotEmpty(elementIds)) {
            elementIdList = Arrays.stream(elementIds.split(",")).map(Integer::parseInt).collect(Collectors.toList());
        }
        List<Map<String, Object>> elementMap = Lists.newArrayList();
        if(StringUtils.isNotEmpty(elementIds)) {
            elementMap = modelMapper.getElementListByIds(elementIdList);
        }
        Map<String,Object> qryParam = Maps.newHashMap();
        List<ExcelExportEntity> excelExportEntities = new ArrayList<ExcelExportEntity>();
        if ("zm".equals(type))
        {
            qryParam.put("tableName","zmzong");
            List<Map<String,Object>> filedInfo = fieldInfoMapper.queryFiledInfo(qryParam);
            // 构造表头
            filedInfo.forEach(field->{
                if(StringUtils.equals("uuid",Objects.toString(field.get("field_name")))){
                    return;
                }
                excelExportEntities.add(new ExcelExportEntity(MapUtils.getString(field,"field_value"),MapUtils.getString(field,"field_name")));
            });
            // 查询数据
            List<Map<String, Object>> result = exportFileMapper.getAllBurmaInformation(elementMap);
            Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("中缅人员信息","中缅人员信息"),excelExportEntities,result);
            ExportUtils.exportExcelFile(response,workbook);
            return;
        }
        qryParam.put("tableName","huizongjichu");
        List<Map<String,Object>> filedInfo = fieldInfoMapper.queryFiledInfo(qryParam);
        // 构造表头
        filedInfo.forEach(field->{
            if(StringUtils.equals("uuid",Objects.toString(field.get("field_name")))){
                return;
            }
            excelExportEntities.add(new ExcelExportEntity(MapUtils.getString(field,"field_value"),MapUtils.getString(field,"field_name")));
        });
        // 查询数据
        List<Map<String, Object>> result = exportFileMapper.getJichuPeopleInformation(elementMap);
        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("疫情人员信息","疫情人员信息"),excelExportEntities,result);
        ExportUtils.exportExcelFile(response,workbook);
    }
}
