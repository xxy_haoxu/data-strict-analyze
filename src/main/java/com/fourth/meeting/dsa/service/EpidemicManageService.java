package com.fourth.meeting.dsa.service;

import com.fourth.meeting.dsa.mapper.EpidemicManageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.Map;

/**
 * @author： wangfan
 * @date： 2020/05/24
 */
@Service
public class EpidemicManageService
{
	@Autowired
	private EpidemicManageMapper epidemicManageMapper;
	 /**
     * 获取信息列表.
     * @param searchModel 搜索内容
     * @return List<Map<String, Object>>
     */
	public Map<String, Object> getAllInformation(Map<String,Object> searchModel)
	{
		Map<String,Object> returnMap = new HashMap<>();
		int total = epidemicManageMapper.getAllInformationCount(searchModel);
		returnMap.put("total",total);
		returnMap.put("list",epidemicManageMapper.getAllInformation(searchModel));
		return returnMap;
	}
	/**
     * 新增人员信息.
     * @param people 人员信息
     * @return 成功/失败
     */
	public String addPeopleInformation(Map<String, Object> people)
	{
		String result = "FAILED";
		int num = epidemicManageMapper.addPeopleInformation(people);
		if (num > 0)
		{
			result = "SUCCESS";
		}
		return result;
	}

	public String addZMPeopleInformation(Map<String, Object> people)
	{
		String result = "FAILED";
		int num = epidemicManageMapper.addZMPeopleInformation(people);
		if (num > 0)
		{
			result = "SUCCESS";
		}
		return result;
	}

	/**
     * 修改人员信息.
     * @param people 人员信息
     * @return 成功/失败
     */
	public String updatePeopleInformation(@RequestParam Map<String, Object> people)
	{
		String result = "FAILED";
		int num = epidemicManageMapper.updatePeopleInformation(people);
		if (num > 0)
		{
			result = "SUCCESS";
		}
		return result;
	}

	public String updateZMPeopleInformation(@RequestParam Map<String, Object> people)
	{
		String result = "FAILED";
		int num = epidemicManageMapper.updateZMPeopleInformation(people);
		if (num > 0)
		{
			result = "SUCCESS";
		}
		return result;
	}
	/**
     * 查询当前人员信息.
     * @param id id
     * @return Map<String, Object>
     */
	public Map<String, Object> getPeopleInformation(String id)
	{
		Map<String, Object> result = new HashMap<>();
		if (StringUtils.isEmpty(id))
		{
			return result;
		}
		return epidemicManageMapper.getPeopleInformation(id);
	}

	public Map<String, Object> getZMPeopleInformation(String id)
	{
		Map<String, Object> result = new HashMap<>();
		if (StringUtils.isEmpty(id))
		{
			return result;
		}
		return epidemicManageMapper.getZMPeopleInformation(id);
	}


	public String deletePeopleInformation(String id)
	{
		String result = "FAILED";
		int num = epidemicManageMapper.deletePeopleInformation(id);
		if (num > 0)
		{
			result = "SUCCESS";
		}
		return result;
	}

	public String deleteZMPeopleInformation(String id)
	{
		String result = "FAILED";
		int num = epidemicManageMapper.deleteZMPeopleInformation(id);
		if (num > 0)
		{
			result = "SUCCESS";
		}
		return result;
	}

	public Map<String, Object> getAllBurmaInformation(Map<String, Object> map)
	{
		Map<String,Object> returnMap = new HashMap<>();
		int total = epidemicManageMapper.getAllBurmaInformationCount(map);
		returnMap.put("total",total);
		returnMap.put("list",epidemicManageMapper.getAllBurmaInformation(map));
		return returnMap;
	}


}
