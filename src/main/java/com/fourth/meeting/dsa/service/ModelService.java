package com.fourth.meeting.dsa.service;

import com.fourth.meeting.dsa.mapper.ModelMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ModelService {
    @Autowired
    private ModelMapper ModelMapper;

    /**
     * 获取組件列表.
     *
     * @param map 搜索内容
     * @return List<Map < String, Object>>
     */
    public Map<String, Object> getElementList(Map<String, Object> map) {
        Map<String, Object> returnMap = new HashMap<>();
        int total = ModelMapper.getElementListCount(map);
        List<Map<String, Object>> elementList = ModelMapper.getElementList(map);
        String modelName = (String) map.getOrDefault("modelName", "");
        String type = (String) map.getOrDefault("type", "");
        if (StringUtils.isNotBlank(modelName)) {
            String elementIds = ModelMapper.getElementIdsByName(modelName,type);
            List<Integer> elementIdList = Arrays.stream(elementIds.split(",")).map(Integer::parseInt).collect(Collectors.toList());
            for (int i = 0; i < elementList.size(); i++) {
                elementList.get(i).put("LAY_CHECKED", false);
                if (elementIdList.contains(elementList.get(i).get("id"))) {
                    elementList.get(i).put("LAY_CHECKED", true);
                }
            }
        }
        returnMap.put("total", total);
        returnMap.put("list", elementList);
        return returnMap;
    }

    /**
     * 获取模塊列表.
     *
     * @param map 搜索内容
     * @return List<Map < String, Object>>
     */
    public Map<String, Object> getModelList(Map<String, Object> map) {
        Map<String, Object> returnMap = new HashMap<>();
        List<Map<String, Object>> returnMapList = new ArrayList<>();
        List<Map<String, Object>> modelList = ModelMapper.getModelList(map);
        for (Map<String, Object> modelInfo : modelList) {
            String elementIds = (String) modelInfo.get("elementIds");
            List<Integer> elementIdList = Arrays.stream(elementIds.split(",")).map(Integer::parseInt).collect(Collectors.toList());
            List<Map<String, Object>> elementMap = ModelMapper.getElementListByIds(elementIdList);
            modelInfo.put("elementInfo", elementMap);
            returnMapList.add(modelInfo);
        }
        int total = ModelMapper.getModelListCount(map);
        returnMap.put("total", total);
        returnMap.put("list", returnMapList);
        return returnMap;
    }

    /**
     * 新增組件信息.
     *
     * @return 成功/失败
     */
    public String addElementInfo(Map<String, Object> map) {
        String result = "FAILED";
        //查詢数据库，防止重名
        int elementCount = ModelMapper.getElementCountByName((String) (map.get("elementName")),(String) (map.get("type")));
        if (elementCount > 0) {
            result = "组件名不能重复";
        } else {
            int num = ModelMapper.addElementInfo(map);
            if (num > 0) {
                result = "SUCCESS";
            }
        }
        return result;
    }

    /**
     * 新增模塊信息.
     *
     * @return 成功/失败
     */
    public String addModelInfo(Map<String, Object> map) {
        String result = "FAILED";
        //查詢数据库，防止重名
        int modelCount = ModelMapper.getModelCountByName((String) (map.get("modelName")),(String) (map.get("type")));
        if (modelCount > 0) {
            result = "模型名不能重复";
        } else {
            int num = ModelMapper.addModelInfo(map);
            if (num > 0) {
                result = "SUCCESS";
            }
        }
        return result;
    }

    /**
     * 修改組件信息.
     *
     * @return 成功/失败
     */
    public String updateElementInfo(Map<String, Object> map) {
        String result = "FAILED";
        int num = ModelMapper.updateElementInfo(map);
        if (num > 0) {
            result = "SUCCESS";
        }
        return result;
    }

    /**
     * 修改模塊信息.
     *
     * @return 成功/失败
     */
    public String updateModelInfo(Map<String, Object> map) {
        String result = "FAILED";
        int num = ModelMapper.updateModelInfo(map);
        if (num > 0) {
            result = "SUCCESS";
        }
        return result;
    }

    /**
     * 刪除組件信息
     *
     * @param id
     * @return
     */
    public String deleteElementInfo(String id) {
        String result = "FAILED";
        int num = ModelMapper.deleteElementInfo(id);
        if (num > 0) {
            result = "SUCCESS";
        }
        return result;
    }

    /**
     * 刪除模塊信息
     *
     * @return
     */
    public String deleteModelInfo(String id) {
        String result = "FAILED";
        int num = ModelMapper.deleteModelInfo(id);
        if (num > 0) {
            result = "SUCCESS";
        }
        return result;
    }


    public Map<String, Object> getAllPersonalList(String elementIds,Integer start,Integer length,String type) {
        Map<String,Object> returnMap = new HashMap<>();
        String tableName = "";
        if(type.equals("yq")){
            tableName = "huizongjichu";
        } else if(type.equals("zm")){
            tableName = "zmzong";
        }
        List<Integer> elementIdList = Arrays.stream(elementIds.split(",")).map(Integer::parseInt).collect(Collectors.toList());
        List<Map<String, Object>> elementMap = ModelMapper.getElementListByIds(elementIdList);
        if(CollectionUtils.isEmpty(elementMap)){
            returnMap.put("total",0);
            returnMap.put("list",new ArrayList<>());
        } else {
            int total = ModelMapper.getAllPersonalListCount(elementMap,tableName);
            returnMap.put("total", total);
            returnMap.put("list", ModelMapper.getAllPersonalList(elementMap, start, length,tableName));
        }
        return returnMap;
    }


    /**
     * 获取字段列表，如果字段是下拉字段，返回下拉信息.
     * @return List<Map<String, Object>>
     */
    public List<Map<String, Object>> getFieldList(String type) {
        String tableName = "";
        if(type.equals("yq")){
            tableName = "huizongjichu";
        } else if(type.equals("zm")){
            tableName = "zmzong";
        }
        List<Map<String, Object>> fieldList = ModelMapper.getFieldList(tableName);
//        for (Map<String, Object> fieldInfo:fieldList) {
//            fieldInfo.put("value",ModelMapper.getFieldValueList(tableName,(String) fieldInfo.get("fieldName")));
//        }
        return fieldList;
    }

    public List<String> getFieldValueList(String type, String fieldName) {
        String tableName = "";
        if (type.equals("yq")) {
            tableName = "huizongjichu";
        } else if (type.equals("zm")) {
            tableName = "zmzong";
        }
        return ModelMapper.getFieldValueList(tableName, fieldName);
    }

    public List<Map<String, Object>> getStatistics(Map<String, Object> map) {
        String elementIds = (String) map.get("elementIds");
        String type= (String)map.get("type");
        List<Integer> elementIdList = Arrays.stream(elementIds.split(",")).map(Integer::parseInt).collect(Collectors.toList());
        List<Map<String, Object>> elementMapList = ModelMapper.getElementListByIds(elementIdList);
        elementMapList = elementMapList.stream().filter(x->MapUtils.isNotEmpty(x)).filter(y->{
            String attributeName = MapUtils.getString(y,"attributeName");
            String attributeValue = MapUtils.getString(y,"attributeValue");
            if(StringUtils.isNotBlank(attributeName)&&StringUtils.isNotBlank(attributeValue)) {
                return true;
            }
            return false;
        }).collect(Collectors.toList());
        String tableName = "";
        if(type.equals("yq")){
            tableName = "huizongjichu";
        } else if(type.equals("zm")){
            tableName = "zmzong";
        }
        List<Map<String,Object>> rtnList = Lists.newArrayList();
        for (Map<String, Object> elementMap:elementMapList) {
            Map<String,Object> rtnOne = Maps.newHashMap();
            rtnOne.put("count",ModelMapper.getStatisticsCount(elementMap,tableName));
            rtnOne.put("elementName",elementMap.get("elementName"));
            rtnList.add(rtnOne);
        }
        return rtnList;
    }
}
