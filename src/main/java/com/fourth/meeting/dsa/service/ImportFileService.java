package com.fourth.meeting.dsa.service;

import com.fourth.meeting.dsa.component.ProgressCache;
import com.fourth.meeting.dsa.component.YqImporter;
import com.fourth.meeting.dsa.component.ZmImporter;
import com.fourth.meeting.dsa.parser.ExcelParseTool;
import com.fourth.meeting.dsa.parser.entity.ExcelParseResult;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;
import sun.rmi.runtime.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
@Service
public class ImportFileService {

    /**
     * 日志.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ImportFileService.class);

    /**
     * 疫情数据导入工具类.
     */
    @Autowired
    private YqImporter yqImporter;


    /**
     * 中缅数据导入.
     */
    @Autowired
    private ZmImporter zmImporter;

    public void yqUpload(MultipartFile multipartFile, Integer headIndex, Integer dataIndex, String progressId) {
        ProgressCache.setId(progressId);
        Map<String,Object> rtn = Maps.newHashMap();
        rtn.put("status",true);
        rtn.put("msg","开始解析");
        ProgressCache.addProgress(ProgressCache.getId(),rtn);
        LOGGER.info("开始解析：{}",progressId);
        if(null == multipartFile){
            rtn.put("status",false);
            rtn.put("msg","文件为空");
            ProgressCache.addProgress(ProgressCache.getId(),rtn);
        }
        // 解析数据
        List<ExcelParseResult> excelData = parseExcel(multipartFile,headIndex,dataIndex);
        if(CollectionUtils.isEmpty(excelData)){
            rtn.put("status",false);
            rtn.put("msg","excel解析为空");
            ProgressCache.addProgress(ProgressCache.getId(),rtn);
        }
        yqImporter.importData(excelData);
        rtn.put("status",true);
        rtn.put("msg","excel解析并入库完成");
        rtn.put("over",true);
        ProgressCache.addProgress(ProgressCache.getId(),rtn);
    }

    public void zmupload(MultipartFile multipartFile, Integer headIndex, Integer dataIndex, String progressId) {
        ProgressCache.setId(progressId);
        Map<String,Object> rtn = Maps.newHashMap();
        rtn.put("status",true);
        rtn.put("msg","开始解析");
        ProgressCache.addProgress(ProgressCache.getId(),rtn);
        if(null == multipartFile){
            rtn.put("status",false);
            rtn.put("msg","文件为空");
            ProgressCache.addProgress(ProgressCache.getId(),rtn);
        }

        // 解析数据
        List<ExcelParseResult> excelData = parseExcel(multipartFile,headIndex,dataIndex);
        if(CollectionUtils.isEmpty(excelData)){
            rtn.put("status",false);
            rtn.put("msg","excel解析为空");
            ProgressCache.addProgress(ProgressCache.getId(),rtn);
        }
        zmImporter.importData(excelData);
        rtn.put("status",true);
        rtn.put("msg","excel解析并入库完成");
        rtn.put("over",true);
        ProgressCache.addProgress(ProgressCache.getId(),rtn);
    }

    /**
     * 解析excel.
     * @param multipartFile 文件.
     * @param headIndex 表头行下标.
     * @param dataIndex 数据行下标.
     * @return 解析得到的数据.每个sheet对应一个ExcelParseResult.
     */
    private List<ExcelParseResult> parseExcel(MultipartFile multipartFile, Integer headIndex, Integer dataIndex)
    {
        // excel文件名.
        String fileName = multipartFile.getOriginalFilename();

        if(StringUtils.isEmpty(fileName)){
            return Lists.newArrayList();
        }
        InputStream excelStream = getExcelStream(multipartFile);
        if(null == excelStream){
            // TODO 待补充异常处理
            return null;
        }

        if(fileName.endsWith(ExcelParseTool.EXCEL_TYPE_XLS)){
            return ExcelParseTool.parseXls(excelStream,headIndex,dataIndex,fileName);
        }
        if(fileName.endsWith(ExcelParseTool.EXCEL_TYPE_XLSX)){
            return ExcelParseTool.parseXlsx(excelStream,headIndex,dataIndex,fileName);
        }
        return null;
    }

    /**
     * 读取文件流.
     * @param multipartFile 文件对象.
     * @return
     */
    private InputStream getExcelStream(MultipartFile multipartFile) {
        if(null == multipartFile){
            return null;
        }
        Map<String,Object> info = Maps.newHashMap();
        try {

            info.put("status",true);
            info.put("msg","正在读取excel");
            ProgressCache.addProgress(ProgressCache.getId(),info);
            InputStream result = multipartFile.getInputStream();
            info.put("msg","读取excel完成");
            ProgressCache.addProgress(ProgressCache.getId(),info);
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            info.put("status",false);
            info.put("msg","读取excel失败");
            ProgressCache.addProgress(ProgressCache.getId(),info);
            return null;
        }

    }
}
