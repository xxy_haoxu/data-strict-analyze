package com.fourth.meeting.dsa.parser.common;

import com.fourth.meeting.dsa.parser.entity.ExcelParseResult;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.MapUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Administrator
 */
public class SheetParser {

    /**
     * 表头的起始下标.
     */
    private int tableHeadIndex;

    /**
     * 数据起始下标.
     */
    private int dataStartIndex;

    /**
     * sheet页签对象.
     */
    private Sheet sheet;

    /**
     * 日志对象.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(SheetParser.class);

    public SheetParser(Sheet sheet, int tableHeadIndex, int dataStartIndex){
        this.tableHeadIndex = tableHeadIndex;
        this.dataStartIndex = dataStartIndex;
        this.sheet = sheet;
    }
    public SheetParser(int tableHeadIndex, int dataStartIndex){
        this.tableHeadIndex = tableHeadIndex;
        this.dataStartIndex = dataStartIndex;
        this.sheet = sheet;
    }

    public void parse(ExcelParseResult excelParseResult) {
        if(null == this.sheet){
            return;
        }
        if(dataStartIndex <= tableHeadIndex){
            LOGGER.warn("表头下不能小于或数据下标");
            return;
        }
        if(null == excelParseResult){
            LOGGER.warn("待赋值的结果对象不能为空.");
            return;
        }
        int firstRowNum = sheet.getFirstRowNum();
        int lastRowNum = sheet.getLastRowNum();
        if(firstRowNum == 0 && lastRowNum == 0){
            System.out.println();
        }
        if(dataStartIndex > lastRowNum){
            excelParseResult.addHintMessage("数据行大于sheet总行数");
            return;
        }
        parseSheetToRow(excelParseResult);

        // 解析结果成功.
        excelParseResult.setParseOk(true);

    }

    private void parseSheetToRow(ExcelParseResult excelParseResult) {
        List<Cell> headerCells = Lists.newArrayList();
        Iterator<Row> rowIterator = this.sheet.iterator();
        int currentRow = 0;
        while (rowIterator.hasNext()){
            Row row = rowIterator.next();
            if(currentRow == tableHeadIndex){
                headerCells = parseRowToCell(row);
                List<String> headers = headerCells.stream().map(Cell::getStringCellValue).collect(Collectors.toList());
                excelParseResult.addHeaders(headers);
                currentRow++;
                continue;
            }
            // 表头没有的情况下，不读取数据.
            if(CollectionUtils.isEmpty(headerCells)){
                currentRow++;
                continue;
            }
            // 没有到数据行的时候也不读取数据
            if(dataStartIndex > currentRow){
                currentRow++;
                continue;
            }

            // 开始解析数据行，得到以Map为格式的数据
            Map<String,Object> oneData = parseData(headerCells,row);
            if(MapUtils.isEmpty(oneData)){
                continue;
            }
            excelParseResult.addExcelData(oneData);
            currentRow++;
        }
    }

    /**
     * 根据表头及对应的数据行，构造数据对象.
     * @param headerCells
     * @param row
     * @return
     */
    private Map<String,Object> parseData(List<Cell> headerCells, Row row) {
        System.out.println(row.getRowNum());
        Map<String,Object> rtnData = Maps.newHashMap();
        for(int i = 0; i < headerCells.size(); i++){
            Cell keyCell = headerCells.get(i);
            try{
                rtnData.put(keyCell.getStringCellValue(),SpecPoiCellUtil.getCellValue(row.getCell(headerCells.get(i).getColumnIndex())));
            } catch (Throwable t){
                t.printStackTrace();
            }
        }
        return rtnData ;
    }


    /**
     * 将row解析个单元格，并放入到list中.
     * @param row
     * @return
     */
    private List<Cell> parseRowToCell(Row row){
        Iterator<Cell> cellIterator = row.cellIterator();
        List<Cell> cells = Lists.newArrayList();
        while (cellIterator.hasNext()){
            cells.add(cellIterator.next());
        }
        return cells;
    }

}
