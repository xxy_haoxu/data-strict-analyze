package com.fourth.meeting.dsa.parser.entity;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
public class ExcelParseResult {

    /**
     * 解析的excel名称.
     */
    private String excelName;

    /**
     * 解析的sheet名称.
     */
    private String sheetName;

    /**
     * 提示信息.
     */
    private List<String> hintMessage = Lists.newArrayList();

    /**
     * 解析是否成功.
     */
    private boolean parseOk = false;

    /**
     * 解析得到的excel表头.
     */
    private List<String> excelHeaders = Lists.newArrayList();

    /**
     * 解析得到的excel数据.
     */
    private List<Map<String,Object>> excelData = Lists.newArrayList();


    /**
     * 添加表头信息.
     * @param headers 表头.
     */
    public void addHeaders(List<String> headers){
        this.excelHeaders.addAll(headers);
    }

    /**
     * 添加表头.
     * @param header 单个表头.
     */
    public void addOneHeader(String header){
        this.excelHeaders.add(header);
    }

    /**
     * 添加解析得到excel信息.
     * @param excelData 解析得到的excel.
     */
    public void addExcelData(Map<String,Object> excelData){
        this.excelData.add(excelData);
    }

    public void addExcelDatas(List<Map<String,Object>> excelDatas){
        this.excelData.addAll(excelDatas);
    }

    public boolean isParseOk() {
        return parseOk;
    }

    public void setParseOk(boolean parseOk) {
        this.parseOk = parseOk;
    }

    public String getExcelName() {
        return excelName;
    }

    public void setExcelName(String excelName) {
        this.excelName = excelName;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    /**
     * 添加提示信息.
     * @param message 提示信息.
     */
    public void addHintMessage(String message){
        this.hintMessage.add(message);
    }

    public List<String> getExcelHeaders() {
        return excelHeaders;
    }

    public List<Map<String, Object>> getExcelData() {
        return excelData;
    }
}
