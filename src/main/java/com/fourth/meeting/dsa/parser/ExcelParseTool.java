package com.fourth.meeting.dsa.parser;

import com.fourth.meeting.dsa.parser.entity.ExcelParseResult;
import com.fourth.meeting.dsa.parser.xls.XlsParser;
import com.fourth.meeting.dsa.parser.xlsx.XlsxParser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.List;

/**
 * @author Administrator
 */
public class ExcelParseTool {

    /**
     * xlsx excel类型解析.
     */
    public static final String EXCEL_TYPE_XLSX = ".xlsx";

    /**
     * xls excel解析.
     */
    public static final String EXCEL_TYPE_XLS = ".xls";

    /**
     * 日志对象.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ExcelParseTool.class);

    /**
     *
     * @param excelStream excel文件流.
     * @param excelType excel 类型：xls,xlsx.
     * @Param startIndex 表头行数.
     * @return excel解析结果.
     */
    public static List<ExcelParseResult> parse(InputStream excelStream, String excelType, int tableHeadIndex, Integer dataStartIndex, String fileName){
        if(StringUtils.isEmpty(excelType)){
            LOGGER.error("文件类型为空");
            return null;
        }
        if(StringUtils.equalsIgnoreCase(excelType,EXCEL_TYPE_XLS)){
            return parseXls(excelStream, tableHeadIndex, dataStartIndex,fileName);
        }
        if(StringUtils.equalsIgnoreCase(excelType, EXCEL_TYPE_XLSX)){
            return parseXlsx(excelStream, tableHeadIndex, dataStartIndex,fileName);
        }
        LOGGER.error("文件类型错误：{}",excelType);
        return null;
    }

    /**
     * 解析格式为xls的excel文件.
     * @param excelStream excel文件流.
     * @return 解析结果，支持多sheet页解析多种类型表头.
     */
    public static List<ExcelParseResult> parseXls(InputStream excelStream, int tableHeadIndex, Integer dataStartIndex, String fileName){
        if(null ==  excelStream){
            return null;
        }
        XlsParser xlsParser = new XlsParser(tableHeadIndex);
        xlsParser.setExcelName(fileName);
        if(null == dataStartIndex){
            xlsParser.setDataStartIndex(++tableHeadIndex);
        } else {
            xlsParser.setDataStartIndex(dataStartIndex);
        }

        return xlsParser.parse(excelStream);
    }

    /**
     * 解析格式为xls的excel文件.
     * @param excelStream excel文件流.
     * @return 解析结果，支持多sheet页解析多种类型表头.
     */
    public static List<ExcelParseResult> parseXlsx(InputStream excelStream, int tableHeadIndex, Integer dataStartIndex, String fileName){
        if(null ==  excelStream){
            return null;
        }

        XlsxParser xlsxParser = new XlsxParser(tableHeadIndex);
        xlsxParser.setExcelName(fileName);
        if(null == dataStartIndex){
            xlsxParser.setDataStartIndex(++tableHeadIndex);
        } else {
            xlsxParser.setDataStartIndex(dataStartIndex);
        }

        return xlsxParser.parse(excelStream);
    }
}
