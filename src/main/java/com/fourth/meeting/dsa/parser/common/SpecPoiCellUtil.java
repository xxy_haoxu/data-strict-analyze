package com.fourth.meeting.dsa.parser.common;

import cn.afterturn.easypoi.util.PoiCellUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;

import java.math.BigDecimal;

/**
 * @author Administrator
 */
public class SpecPoiCellUtil extends PoiCellUtil {
    /**
     * 获取单元格的值
     *
     * @param cell
     * @return
     */
    public static String getCellValue(Cell cell) {
        if (cell == null) {
            return "";
        }

        if (cell.getCellType() == CellType.STRING) {

            return cell.getStringCellValue();

        } else if (cell.getCellType() == CellType.BOOLEAN) {

            return String.valueOf(cell.getBooleanCellValue());

        } else if (cell.getCellType() == CellType.FORMULA) {

            try {
                return cell.getCellFormula();
            } catch (Exception e) {
                return String.valueOf(cell.getNumericCellValue());
            }

        } else if (cell.getCellType() == CellType.NUMERIC) {
            BigDecimal bd = new BigDecimal(cell.getNumericCellValue());
            bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
            String rtnValue =String.valueOf(bd);
            if(StringUtils.isBlank(rtnValue)){
                return rtnValue;
            }
            if(StringUtils.endsWith(rtnValue,".00")){
                return rtnValue.replace(".00","");
            }
            return rtnValue;
        } else {
            cell.setCellType(CellType.STRING);
            return cell.getStringCellValue();
        }
    }
}
