package com.fourth.meeting.dsa.parser.xlsx;

import com.fourth.meeting.dsa.component.ProgressCache;
import com.fourth.meeting.dsa.parser.common.SheetParser;
import com.fourth.meeting.dsa.parser.entity.ExcelParseResult;
import com.google.common.collect.Maps;
import org.apache.commons.compress.utils.Lists;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
public class XlsxParser {

    private static final int DEFAULT_TABLE_HEAD_INDEX = 1;

    /**
     * 日志对象.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(XlsxParser.class);

    /**
     * 表头行下标，默认第一行为表头.
     */
    private int tableHeadIndex;

    /**
     * 数据起始下标.
     */
    private int dataStartIndex;

    /**
     * excel文件名.
     */
    private String excelName;

    public XlsxParser(){
        this.tableHeadIndex = DEFAULT_TABLE_HEAD_INDEX;
    }

    public XlsxParser(int tableHeadIndex) {
        this.tableHeadIndex = tableHeadIndex;
    }

    /**
     *
     * @param excelStream
     * @return 多个sheet返回多种类型的表头，也可以表头相同.
     */
    public List<ExcelParseResult> parse(InputStream excelStream) {
        Map<String,Object> info = Maps.newHashMap();
        try {
            info.put("status",true);
            info.put("msg","开始解析excel");
            ProgressCache.addProgress(ProgressCache.getId(),info);
            Workbook workbook = XSSFWorkbookFactory.create(excelStream);
            List<ExcelParseResult> parsedResult = Lists.newArrayList();
            Iterator<Sheet> sheetIterator = workbook.sheetIterator();
            while (sheetIterator.hasNext()){
                ExcelParseResult excelParseResult = new ExcelParseResult();
                Sheet sheet = sheetIterator.next();
                excelParseResult.setExcelName(this.getExcelName());
                excelParseResult.setSheetName(sheet.getSheetName());
                new SheetParser(sheet,this.tableHeadIndex,this.dataStartIndex).parse(excelParseResult);
                if(null == excelParseResult || !excelParseResult.isParseOk()){
                    LOGGER.warn("解析sheet失败,sheet名为:{}",sheet.getSheetName());
                    continue;
                }

                parsedResult.add(excelParseResult);
            }
            info.put("status",true);
            info.put("msg","解析excel完成");
            ProgressCache.addProgress(ProgressCache.getId(),info);
            return parsedResult;
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            info.put("status",false);
            info.put("msg","解析excel失败");
            ProgressCache.addProgress(ProgressCache.getId(),info);
            return null;
        }
    }

    public int getDataStartIndex() {
        return dataStartIndex;
    }

    public void setDataStartIndex(int dataStartIndex) {
        this.dataStartIndex = dataStartIndex;
    }

    public String getExcelName() {
        return excelName;
    }

    public void setExcelName(String excelName) {
        this.excelName = excelName;
    }
}
